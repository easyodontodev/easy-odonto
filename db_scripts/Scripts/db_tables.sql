drop DATABASE IF EXISTS odt_soft;

CREATE DATABASE odt_soft	

DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE odt_soft;	


CREATE TABLE users (

	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  	`name` varchar(80) NOT NULL,
	`surname` varchar(80) NOT NULL,
	`cro` varchar(10),
	`email`varchar(80) NOT NULL,
	`registration` int (11) NOT NULL,
	`image` text,  
	`phone` varchar (20) NOT NULL,
	`phone2` varchar(20), 
	`address` varchar(80) NOT NULL,
	`admission_date` date NOT NULL,
	`password` varchar(100) NOT NULL,
	`social_security` varchar (50) NOT NULL,
	`bank` varchar (80) NOT NULL,
	`number_of_account` varchar (20) NOT NULL,
	`agency` varchar(80) NOT NULL,
	`permition` int (11) NOT NULL,
	`deleted` boolean,
    `time` timestamp

) ENGINE=InnoDB;

CREATE TABLE users_access_hospital( 
	
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`users` int (11) NOT NULL,
	`hospital` int (11) NOT NULL,
	`time` timestamp
    
) ENGINE=InnoDB;

CREATE TABLE permition(

	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`type` varchar(40) NOT NULL
    
) ENGINE=InnoDB;



CREATE TABLE patient(

	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`name` varchar (160) NOT NULL,
	`email` varchar (80),
	`social_security` varchar (20) NOT NULL,
	`birthdate` date NOT NULL,
	`gender` varchar (10) NOT NULL,
	`address` varchar(80), 
	`neighborhood` varchar(80),
	`city` varchar(80),
	`state` varchar(80),
	`zip_code` varchar(20),
	`health_insurance` int NOT NULL,
	`responsible1` varchar(80),
	`telephone_r1` varchar(20),            
	`responsible2` varchar(80),
	`telephone_r2` varchar(20),	
	`clinic` boolean,
	`name_phy_assistant` varchar(80),
	`telephone_phy_assistant` varchar(20),
	`speciality_phy_assistant` varchar(80),
	`deleted` boolean,
    `time` timestamp
    
) ENGINE=InnoDB;

CREATE TABLE hospital(

	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` varchar(80) NOT NULL,
	`image` text ,
	`telephone_uti`varchar(20) NOT NULL,
	`telephone_chefe_uti`varchar(20) NOT NULL,
	`nome_chefe_uti`varchar(80) NOT NULL,
	`deleted` boolean NOT NULL,
    `file` text,
    `time` timestamp
    
) ENGINE=InnoDB;



CREATE TABLE  stats(

	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`status` varchar (80) NOT NULL
    
)ENGINE=InnoDB;
	

CREATE TABLE hospital_itu(

	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name_itu` varchar(80) NOT NULL,
	`quant_itu` int(5) NOT NULL,
	`hospital` int(11) NOT NULL,
    `time` timestamp
    
)ENGINE=InnoDB;
	
	
CREATE TABLE appointment(
	
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`description` text ,
	`status` int (3) NOT NULL ,	 
	`attendance`int (11) NOT NULL,
    `time` timestamp
    
)ENGINE=InnoDB; 


CREATE TABLE attendance(

	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`patient`  int NOT NULL,
	`hospital` int NOT NULL,
	`bed` int,
	`status` int Not Null,
	`admission_date_itu` datetime NOT NULL,
	`doctor_responsible` int  NOT NULL,
    `initial_date` date NOT NULL, 
	`final_date` date,
    `time` timestamp

) ENGINE=InnoDB; 


CREATE TABLE admission_cause(

	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`type` varchar (80) NOT NULL
    
) ENGINE=InnoDB;


CREATE TABLE itu_bed(
	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name_bed` varchar(80),
	`number_itu` int (11) NOT NULL,
	`itu` int (11) NOT NULL,
    `time` timestamp
    
) ENGINE=InnoDB;


CREATE TABLE documents(
	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` varchar (60) NOT NULL,
	`URI` text NOT NULL,
	`doc_type` int (11) NOT NULL,
    `time` timestamp
	
) ENGINE=InnoDB;


CREATE TABLE meta_entity( 
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` varchar (63) NOT NULL
    
) ENGINE=InnoDB;


CREATE TABLE entity_has_document( 
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`documents` int (11) NOT NULL,
	`meta_entity` int (11) NOT NULL,
	`entity` int (11) NOT NULL,
    `time` timestamp
    
) ENGINE=InnoDB;

CREATE TABLE doc_type(
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` varchar (60) NOT NULL
    
) ENGINE=InnoDB;

CREATE TABLE attendance_cause(
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`attendance` int NOT NULL,
	`admission_cause` int NOT NULL,
    `time` timestamp
    
) ENGINE=InnoDB;

CREATE TABLE health_insurance(

	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `description` varchar(80)

) ENGINE=InnoDB;
 
CREATE TABLE evaluation(
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `patient` INT(11) NOT NULL,
    `headbord` varchar(10),
    `diet` varchar(10),
    `conscious_oriented` varchar(10),
	`odt` varchar(10),
    `iho_pc` int(11),
    `ventilation` varchar(10),
    `mon_odt` varchar(10),
    `time` timestamp
);

CREATE TABLE iho_pc(
  `id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(80)
);

CREATE TABLE patient_access_evaluation( 
	`id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`patient` int (11) NOT NULL,
	`evaluation` int (11) NOT NULL,
	`time` timestamp
    
) ENGINE=InnoDB;

/* FOREIGN KEY */

ALTER TABLE patient
ADD CONSTRAINT fk_health_insurance FOREIGN KEY (health_insurance) REFERENCES health_insurance(id);

ALTER TABLE evaluation
ADD CONSTRAINT fk_evaluation_patient FOREIGN KEY (patient) REFERENCES patient(id);

ALTER TABLE evaluation
ADD CONSTRAINT fk_iho_pc FOREIGN KEY (iho_pc) REFERENCES iho_pc(id);

ALTER TABLE attendance_cause
ADD CONSTRAINT fk_attendance_cause FOREIGN KEY (attendance) REFERENCES attendance(id);

ALTER TABLE attendance_cause
ADD CONSTRAINT fk_admission_cause FOREIGN KEY (admission_cause) REFERENCES admission_cause(id);

ALTER TABLE attendance
ADD CONSTRAINT fk_doctor_responsable FOREIGN KEY (doctor_responsible) REFERENCES users(id);

ALTER TABLE users 
ADD CONSTRAINT fk_permition FOREIGN KEY (permition) REFERENCES permition(id);

ALTER TABLE attendance
ADD CONSTRAINT fk_status_attendance FOREIGN KEY (status) REFERENCES stats(id); 

ALTER TABLE attendance
ADD CONSTRAINT fk_hospital FOREIGN KEY (hospital) REFERENCES hospital(id);

ALTER TABLE attendance
ADD CONSTRAINT fk_bed FOREIGN KEY (bed) REFERENCES itu_bed(id);

ALTER TABLE attendance 
ADD CONSTRAINT fk_patient FOREIGN KEY (patient) REFERENCES patient(id);

ALTER TABLE hospital_itu
ADD CONSTRAINT fk_hospital_itu FOREIGN KEY (hospital) REFERENCES hospital(id);

ALTER TABLE appointment	
ADD CONSTRAINT fk_attendance FOREIGN KEY (attendance) REFERENCES attendance(id);

ALTER TABLE itu_bed
ADD CONSTRAINT fk_itu_bed FOREIGN KEY (itu) REFERENCES hospital_itu(id);

ALTER TABLE appointment 
ADD CONSTRAINT fk_status FOREIGN KEY (status) REFERENCES stats(id);

ALTER TABLE users_access_hospital 
ADD CONSTRAINT fk_users FOREIGN KEY (users) REFERENCES users(id);

ALTER TABLE users_access_hospital 
ADD CONSTRAINT fk_hospital_access FOREIGN KEY (hospital) REFERENCES hospital(id);

ALTER TABLE entity_has_document
ADD CONSTRAINT fk_entity_has_document_document FOREIGN KEY (documents) REFERENCES documents(id);

ALTER TABLE entity_has_document
ADD CONSTRAINT fk_entity_has_document_meta_entity FOREIGN KEY (meta_entity) REFERENCES meta_entity(id);

ALTER TABLE documents
ADD CONSTRAINT fk_doc_type FOREIGN KEY (doc_type) REFERENCES doc_type(id);

ALTER TABLE patient_access_evaluation 
ADD CONSTRAINT fk_patient_access FOREIGN KEY (patient) REFERENCES patient(id);

ALTER TABLE patient_access_evaluation 
ADD CONSTRAINT fk_evaluation_access FOREIGN KEY (evaluation) REFERENCES evaluation(id);

/*INSERTS*/

INSERT INTO permition (type)
VALUES ('Dentista Administrador') , ('Dentista') , ('Administrador') , ('Secretaria');

INSERT INTO admission_cause (type)
VALUES ('Oncologico') , ('Cardiopata') , ('Cirurgia') , ('Paliativo') , ('Urgente') , ('Quimioterapia') , ('Pulmonar') , ('IRC') , ('Neuropata');

INSERT INTO `users`
(`name`,`surname`,`cro`,`email`,`registration`,`phone`,`phone2`,`address`,`admission_date`,`password`,`social_security`,`bank`,`number_of_account`,`agency`,`permition`,`deleted`)
VALUES('odonto','teste','12345','teste@gmail.com',123,'123','123','SCS','2017/03/03','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','456789','BANCO','99','987',3,false);

INSERT INTO `users`
(`name`,`surname`,`cro`,`email`,`registration`,`phone`,`phone2`,`address`,`admission_date`,`password`,`social_security`,`bank`,`number_of_account`,`agency`,`permition`,`deleted`)
VALUES('Renata','Monteiro de Paula Sgarioni','12345','renata@amareodontologia.com.br',123,'123','123','SCS','2017/03/03','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','456789','BANCO','99','987',3,false);

INSERT INTO stats (status) 
VALUES ('Em avaliação') , ('Em reavaliação') , ('Em aprovação') , ('Aprovado pelo médico responsável') , ('Aprovado pela família') , ('Em orçamento') , ('Orçamento aprovado pela família') , ('Anexa parecer'),
('Em acompanhamento') , ('Dispensa') , ('Óbito') , ('Alta') , ('Concluído') ;

INSERT INTO meta_entity(name)
VALUES ('Hospital') , ('Paciente') , ('Funcionário') , ('Atendimento');

INSERT INTO doc_type (name)
VALUES ('.txt'),('.pdf'),('.docx'),('.doc'),('.xlsx'),('.xls'),('.ppt'),('pptx'), ('.jpeg'), ('.png');

INSERT INTO health_insurance (description)
VALUES ('Allianz'), ('Amil'), ('Assefaz'), ('Bacen'), ('Bombeiros'), ('Bradesco'), ('BRB'), ('Capesaude'), ('Casembrapa'), ('Cassi'), ('FACEB'), ('GEAP'), ('Golden'), ('MPDFT'), ('MPF'),
('MPM'), ('Outros'), ('PAME'), ('Porto Seguro'), ('Rede Gama'), ('Saúde Caixa'), ('Saúde Sim'), ('SLAM'), ('STJ'), ('STM'), ('Sul América'), ('TJDF'), ('TRE'), ('TRT'), ('Unimed');

INSERT INTO iho_pc (type)
VALUES ('Gengivite'), ('Placa Bacteriana'), ('Saburra'), ('Halitose'), ('Secreção'), ('Sangue'), ('Dieta'), ('Fatores de Retenção');

INSERT INTO `hospital`
(`id`, `name`, `image`, `telephone_uti`, `telephone_chefe_uti`, `nome_chefe_uti`, `deleted`, `file`, `time`)
VALUES
	(1, 'Hospital das Forças Armadas', '../images/hospital/be22595202b4.jpg', '(61) 3212-4565', '(61) 99557-7456', 'General Força', 0, NULL, '2017-11-24 00:23:21'),
	(2, 'Hospital Santa Helena', '../images/hospital/7cfc58fe52c5.jpg', '(61) 2154-8789', '(61) 98456-7788', 'Maria Lurdes', 0, NULL, '2017-11-24 00:26:21'),
	(3, 'Hospital Santa Lucia', '../images/hospital/d84144556d4b.jpg', '(61) 2514-3614', '(61) 98889-9775', 'Lucia Almeida', 0, NULL, '2017-11-24 00:29:09'),
	(4, 'Hospital Vera Cruz', '../images/hospital/abe8e5756772.jpg', '(19) 2255-4632', '(19) 98722-3314', 'Jorge Farias', 0, NULL, '2017-11-24 00:32:16'),
	(5, 'Hospital Pasteur', '../images/hospital/2e0306a2c530.jpg', '(21) 2255-3214', '(21) 99885-4653', 'Jorge Lafont', 0, NULL, '2017-11-24 00:35:31'),
	(6, 'Hospital HOME', '../images/hospital/c9a2a6f279f4.jpg', '(61) 2255-4623', '(61) 98875-4335', 'Cristina Almeida', 0, NULL, '2017-11-24 00:37:40'),
	(7, 'Hospital Santa Luzia', '../images/hospital/daf6c9721439.jpg', '(61) 2244-6459', '(61) 99773-2345', 'Luiz Farias', 0, NULL, '2017-11-24 00:58:55'),
	(8, 'Hospital do Coração do Brasil', '../images/hospital/5d98981444a7.jpg', '(61) 2324-3656', '(61) 93364-8678', 'Pedro Coentro', 0, NULL, '2017-11-24 01:02:26'),
	(9, 'Hospital Alvorada Brasília', '../images/hospital/e3d40ba4bb66.jpg', '(61) 3234-7364', '(61) 99373-2664', 'Gabriel Linhares', 0, NULL, '2017-11-24 01:04:31');
    
INSERT INTO `hospital_itu`
(`id`, `name_itu`, `quant_itu`, `hospital`, `time`)
VALUES
	(1, 'UTI Teste', 20, 1, '2017-11-24 00:23:21'),
	(2, 'UTI Teste 2', 12, 1, '2017-11-24 00:23:22'),
	(3, 'Ala Sul', 15, 2, '2017-11-24 00:26:21'),
	(4, 'Ala Norte', 20, 2, '2017-11-24 00:26:21'),
	(5, 'Inferior', 8, 3, '2017-11-24 00:29:09'),
	(6, 'Superior', 8, 3, '2017-11-24 00:29:09'),
	(7, 'Bloco A', 14, 4, '2017-11-24 00:32:16'),
	(8, 'Bloco B', 14, 4, '2017-11-24 00:32:17'),
	(9, 'Bloco C', 14, 4, '2017-11-24 00:32:18'),
	(10, 'Bloco D', 14, 4, '2017-11-24 00:32:18'),
	(11, 'UTI 1', 8, 5, '2017-11-24 00:35:31'),
	(12, 'UTI 2', 8, 5, '2017-11-24 00:35:31'),
	(13, 'UTI 3', 8, 5, '2017-11-24 00:35:32'),
	(14, 'Principal', 5, 6, '2017-11-24 00:37:41'),
	(15, 'Secundária', 5, 6, '2017-11-24 00:37:41'),
	(16, 'UTI A', 10, 7, '2017-11-24 00:58:55'),
	(17, 'UTI B', 10, 7, '2017-11-24 00:58:55'),
	(18, 'UTI C', 10, 7, '2017-11-24 00:58:56'),
	(19, 'Asa Sul', 16, 8, '2017-11-24 01:02:26'),
	(20, 'Asa Norte', 16, 8, '2017-11-24 01:02:27'),
	(21, 'A', 20, 9, '2017-11-24 01:04:31'),
	(22, 'B', 20, 9, '2017-11-24 01:04:32');

INSERT INTO `itu_bed`
(`id`, `name_bed`, `number_itu`, `itu`, `time`)
VALUES
	(1, NULL, 1, 1, '2017-11-24 00:23:21'),
	(2, NULL, 2, 1, '2017-11-24 00:23:21'),
	(3, NULL, 3, 1, '2017-11-24 00:23:21'),
	(4, NULL, 4, 1, '2017-11-24 00:23:21'),
	(5, NULL, 5, 1, '2017-11-24 00:23:21'),
	(6, NULL, 6, 1, '2017-11-24 00:23:21'),
	(7, NULL, 7, 1, '2017-11-24 00:23:21'),
	(8, NULL, 8, 1, '2017-11-24 00:23:21'),
	(9, NULL, 9, 1, '2017-11-24 00:23:21'),
	(10, NULL, 10, 1, '2017-11-24 00:23:21'),
	(11, NULL, 11, 1, '2017-11-24 00:23:21'),
	(12, NULL, 12, 1, '2017-11-24 00:23:21'),
	(13, NULL, 13, 1, '2017-11-24 00:23:22'),
	(14, NULL, 14, 1, '2017-11-24 00:23:22'),
	(15, NULL, 15, 1, '2017-11-24 00:23:22'),
	(16, NULL, 16, 1, '2017-11-24 00:23:22'),
	(17, NULL, 17, 1, '2017-11-24 00:23:22'),
	(18, NULL, 18, 1, '2017-11-24 00:23:22'),
	(19, NULL, 19, 1, '2017-11-24 00:23:22'),
	(20, NULL, 20, 1, '2017-11-24 00:23:22'),
	(21, NULL, 1, 2, '2017-11-24 00:23:22'),
	(22, NULL, 2, 2, '2017-11-24 00:23:22'),
	(23, NULL, 3, 2, '2017-11-24 00:23:22'),
	(24, NULL, 4, 2, '2017-11-24 00:23:22'),
	(25, NULL, 5, 2, '2017-11-24 00:23:22'),
	(26, NULL, 6, 2, '2017-11-24 00:23:22'),
	(27, NULL, 7, 2, '2017-11-24 00:23:22'),
	(28, NULL, 8, 2, '2017-11-24 00:23:22'),
	(29, NULL, 9, 2, '2017-11-24 00:23:22'),
	(30, NULL, 10, 2, '2017-11-24 00:23:22'),
	(31, NULL, 11, 2, '2017-11-24 00:23:22'),
	(32, NULL, 12, 2, '2017-11-24 00:23:22'),
	(33, NULL, 1, 3, '2017-11-24 00:26:21'),
	(34, NULL, 2, 3, '2017-11-24 00:26:21'),
	(35, NULL, 3, 3, '2017-11-24 00:26:21'),
	(36, NULL, 4, 3, '2017-11-24 00:26:21'),
	(37, NULL, 5, 3, '2017-11-24 00:26:21'),
	(38, NULL, 6, 3, '2017-11-24 00:26:21'),
	(39, NULL, 7, 3, '2017-11-24 00:26:21'),
	(40, NULL, 8, 3, '2017-11-24 00:26:21'),
	(41, NULL, 9, 3, '2017-11-24 00:26:21'),
	(42, NULL, 10, 3, '2017-11-24 00:26:21'),
	(43, NULL, 11, 3, '2017-11-24 00:26:21'),
	(44, NULL, 12, 3, '2017-11-24 00:26:21'),
	(45, NULL, 13, 3, '2017-11-24 00:26:21'),
	(46, NULL, 14, 3, '2017-11-24 00:26:21'),
	(47, NULL, 15, 3, '2017-11-24 00:26:21'),
	(48, NULL, 1, 4, '2017-11-24 00:26:22'),
	(49, NULL, 2, 4, '2017-11-24 00:26:22'),
	(50, NULL, 3, 4, '2017-11-24 00:26:22'),
	(51, NULL, 4, 4, '2017-11-24 00:26:22'),
	(52, NULL, 5, 4, '2017-11-24 00:26:22'),
	(53, NULL, 6, 4, '2017-11-24 00:26:22'),
	(54, NULL, 7, 4, '2017-11-24 00:26:22'),
	(55, NULL, 8, 4, '2017-11-24 00:26:22'),
	(56, NULL, 9, 4, '2017-11-24 00:26:22'),
	(57, NULL, 10, 4, '2017-11-24 00:26:22'),
	(58, NULL, 11, 4, '2017-11-24 00:26:22'),
	(59, NULL, 12, 4, '2017-11-24 00:26:22'),
	(60, NULL, 13, 4, '2017-11-24 00:26:22'),
	(61, NULL, 14, 4, '2017-11-24 00:26:22'),
	(62, NULL, 15, 4, '2017-11-24 00:26:22'),
	(63, NULL, 16, 4, '2017-11-24 00:26:22'),
	(64, NULL, 17, 4, '2017-11-24 00:26:22'),
	(65, NULL, 18, 4, '2017-11-24 00:26:22'),
	(66, NULL, 19, 4, '2017-11-24 00:26:22'),
	(67, NULL, 20, 4, '2017-11-24 00:26:22'),
	(68, NULL, 1, 5, '2017-11-24 00:29:09'),
	(69, NULL, 2, 5, '2017-11-24 00:29:09'),
	(70, NULL, 3, 5, '2017-11-24 00:29:09'),
	(71, NULL, 4, 5, '2017-11-24 00:29:09'),
	(72, NULL, 5, 5, '2017-11-24 00:29:09'),
	(73, NULL, 6, 5, '2017-11-24 00:29:09'),
	(74, NULL, 7, 5, '2017-11-24 00:29:09'),
	(75, NULL, 8, 5, '2017-11-24 00:29:09'),
	(76, NULL, 1, 6, '2017-11-24 00:29:09'),
	(77, NULL, 2, 6, '2017-11-24 00:29:10'),
	(78, NULL, 3, 6, '2017-11-24 00:29:10'),
	(79, NULL, 4, 6, '2017-11-24 00:29:10'),
	(80, NULL, 5, 6, '2017-11-24 00:29:10'),
	(81, NULL, 6, 6, '2017-11-24 00:29:10'),
	(82, NULL, 7, 6, '2017-11-24 00:29:10'),
	(83, NULL, 8, 6, '2017-11-24 00:29:10'),
	(84, NULL, 1, 7, '2017-11-24 00:32:16'),
	(85, NULL, 2, 7, '2017-11-24 00:32:16'),
	(86, NULL, 3, 7, '2017-11-24 00:32:16'),
	(87, NULL, 4, 7, '2017-11-24 00:32:16'),
	(88, NULL, 5, 7, '2017-11-24 00:32:17'),
	(89, NULL, 6, 7, '2017-11-24 00:32:17'),
	(90, NULL, 7, 7, '2017-11-24 00:32:17'),
	(91, NULL, 8, 7, '2017-11-24 00:32:17'),
	(92, NULL, 9, 7, '2017-11-24 00:32:17'),
	(93, NULL, 10, 7, '2017-11-24 00:32:17'),
	(94, NULL, 11, 7, '2017-11-24 00:32:17'),
	(95, NULL, 12, 7, '2017-11-24 00:32:17'),
	(96, NULL, 13, 7, '2017-11-24 00:32:17'),
	(97, NULL, 14, 7, '2017-11-24 00:32:17'),
	(98, NULL, 1, 8, '2017-11-24 00:32:17'),
	(99, NULL, 2, 8, '2017-11-24 00:32:17'),
	(100, NULL, 3, 8, '2017-11-24 00:32:17'),
	(101, NULL, 4, 8, '2017-11-24 00:32:17'),
	(102, NULL, 5, 8, '2017-11-24 00:32:17'),
	(103, NULL, 6, 8, '2017-11-24 00:32:17'),
	(104, NULL, 7, 8, '2017-11-24 00:32:17'),
	(105, NULL, 8, 8, '2017-11-24 00:32:17'),
	(106, NULL, 9, 8, '2017-11-24 00:32:17'),
	(107, NULL, 10, 8, '2017-11-24 00:32:17'),
	(108, NULL, 11, 8, '2017-11-24 00:32:18'),
	(109, NULL, 12, 8, '2017-11-24 00:32:18'),
	(110, NULL, 13, 8, '2017-11-24 00:32:18'),
	(111, NULL, 14, 8, '2017-11-24 00:32:18'),
	(112, NULL, 1, 9, '2017-11-24 00:32:18'),
	(113, NULL, 2, 9, '2017-11-24 00:32:18'),
	(114, NULL, 3, 9, '2017-11-24 00:32:18'),
	(115, NULL, 4, 9, '2017-11-24 00:32:18'),
	(116, NULL, 5, 9, '2017-11-24 00:32:18'),
	(117, NULL, 6, 9, '2017-11-24 00:32:18'),
	(118, NULL, 7, 9, '2017-11-24 00:32:18'),
	(119, NULL, 8, 9, '2017-11-24 00:32:18'),
	(120, NULL, 9, 9, '2017-11-24 00:32:18'),
	(121, NULL, 10, 9, '2017-11-24 00:32:18'),
	(122, NULL, 11, 9, '2017-11-24 00:32:18'),
	(123, NULL, 12, 9, '2017-11-24 00:32:18'),
	(124, NULL, 13, 9, '2017-11-24 00:32:18'),
	(125, NULL, 14, 9, '2017-11-24 00:32:18'),
	(126, NULL, 1, 10, '2017-11-24 00:32:18'),
	(127, NULL, 2, 10, '2017-11-24 00:32:18'),
	(128, NULL, 3, 10, '2017-11-24 00:32:18'),
	(129, NULL, 4, 10, '2017-11-24 00:32:18'),
	(130, NULL, 5, 10, '2017-11-24 00:32:18'),
	(131, NULL, 6, 10, '2017-11-24 00:32:19'),
	(132, NULL, 7, 10, '2017-11-24 00:32:19'),
	(133, NULL, 8, 10, '2017-11-24 00:32:19'),
	(134, NULL, 9, 10, '2017-11-24 00:32:19'),
	(135, NULL, 10, 10, '2017-11-24 00:32:19'),
	(136, NULL, 11, 10, '2017-11-24 00:32:19'),
	(137, NULL, 12, 10, '2017-11-24 00:32:19'),
	(138, NULL, 13, 10, '2017-11-24 00:32:19'),
	(139, NULL, 14, 10, '2017-11-24 00:32:19'),
	(140, NULL, 1, 11, '2017-11-24 00:35:31'),
	(141, NULL, 2, 11, '2017-11-24 00:35:31'),
	(142, NULL, 3, 11, '2017-11-24 00:35:31'),
	(143, NULL, 4, 11, '2017-11-24 00:35:31'),
	(144, NULL, 5, 11, '2017-11-24 00:35:31'),
	(145, NULL, 6, 11, '2017-11-24 00:35:31'),
	(146, NULL, 7, 11, '2017-11-24 00:35:31'),
	(147, NULL, 8, 11, '2017-11-24 00:35:31'),
	(148, NULL, 1, 12, '2017-11-24 00:35:31'),
	(149, NULL, 2, 12, '2017-11-24 00:35:31'),
	(150, NULL, 3, 12, '2017-11-24 00:35:31'),
	(151, NULL, 4, 12, '2017-11-24 00:35:31'),
	(152, NULL, 5, 12, '2017-11-24 00:35:31'),
	(153, NULL, 6, 12, '2017-11-24 00:35:32'),
	(154, NULL, 7, 12, '2017-11-24 00:35:32'),
	(155, NULL, 8, 12, '2017-11-24 00:35:32'),
	(156, NULL, 1, 13, '2017-11-24 00:35:32'),
	(157, NULL, 2, 13, '2017-11-24 00:35:32'),
	(158, NULL, 3, 13, '2017-11-24 00:35:32'),
	(159, NULL, 4, 13, '2017-11-24 00:35:32'),
	(160, NULL, 5, 13, '2017-11-24 00:35:32'),
	(161, NULL, 6, 13, '2017-11-24 00:35:32'),
	(162, NULL, 7, 13, '2017-11-24 00:35:32'),
	(163, NULL, 8, 13, '2017-11-24 00:35:32'),
	(164, NULL, 1, 14, '2017-11-24 00:37:41'),
	(165, NULL, 2, 14, '2017-11-24 00:37:41'),
	(166, NULL, 3, 14, '2017-11-24 00:37:41'),
	(167, NULL, 4, 14, '2017-11-24 00:37:41'),
	(168, NULL, 5, 14, '2017-11-24 00:37:41'),
	(169, NULL, 1, 15, '2017-11-24 00:37:41'),
	(170, NULL, 2, 15, '2017-11-24 00:37:41'),
	(171, NULL, 3, 15, '2017-11-24 00:37:41'),
	(172, NULL, 4, 15, '2017-11-24 00:37:41'),
	(173, NULL, 5, 15, '2017-11-24 00:37:41'),
	(174, NULL, 1, 16, '2017-11-24 00:58:55'),
	(175, NULL, 2, 16, '2017-11-24 00:58:55'),
	(176, NULL, 3, 16, '2017-11-24 00:58:55'),
	(177, NULL, 4, 16, '2017-11-24 00:58:55'),
	(178, NULL, 5, 16, '2017-11-24 00:58:55'),
	(179, NULL, 6, 16, '2017-11-24 00:58:55'),
	(180, NULL, 7, 16, '2017-11-24 00:58:55'),
	(181, NULL, 8, 16, '2017-11-24 00:58:55'),
	(182, NULL, 9, 16, '2017-11-24 00:58:55'),
	(183, NULL, 10, 16, '2017-11-24 00:58:55'),
	(184, NULL, 1, 17, '2017-11-24 00:58:55'),
	(185, NULL, 2, 17, '2017-11-24 00:58:55'),
	(186, NULL, 3, 17, '2017-11-24 00:58:55'),
	(187, NULL, 4, 17, '2017-11-24 00:58:55'),
	(188, NULL, 5, 17, '2017-11-24 00:58:55'),
	(189, NULL, 6, 17, '2017-11-24 00:58:55'),
	(190, NULL, 7, 17, '2017-11-24 00:58:56'),
	(191, NULL, 8, 17, '2017-11-24 00:58:56'),
	(192, NULL, 9, 17, '2017-11-24 00:58:56'),
	(193, NULL, 10, 17, '2017-11-24 00:58:56'),
	(194, NULL, 1, 18, '2017-11-24 00:58:56'),
	(195, NULL, 2, 18, '2017-11-24 00:58:56'),
	(196, NULL, 3, 18, '2017-11-24 00:58:56'),
	(197, NULL, 4, 18, '2017-11-24 00:58:56'),
	(198, NULL, 5, 18, '2017-11-24 00:58:56'),
	(199, NULL, 6, 18, '2017-11-24 00:58:56'),
	(200, NULL, 7, 18, '2017-11-24 00:58:56'),
	(201, NULL, 8, 18, '2017-11-24 00:58:56'),
	(202, NULL, 9, 18, '2017-11-24 00:58:56'),
	(203, NULL, 10, 18, '2017-11-24 00:58:56'),
	(204, NULL, 1, 19, '2017-11-24 01:02:26'),
	(205, NULL, 2, 19, '2017-11-24 01:02:26'),
	(206, NULL, 3, 19, '2017-11-24 01:02:26'),
	(207, NULL, 4, 19, '2017-11-24 01:02:26'),
	(208, NULL, 5, 19, '2017-11-24 01:02:26'),
	(209, NULL, 6, 19, '2017-11-24 01:02:26'),
	(210, NULL, 7, 19, '2017-11-24 01:02:26'),
	(211, NULL, 8, 19, '2017-11-24 01:02:26'),
	(212, NULL, 9, 19, '2017-11-24 01:02:26'),
	(213, NULL, 10, 19, '2017-11-24 01:02:26'),
	(214, NULL, 11, 19, '2017-11-24 01:02:26'),
	(215, NULL, 12, 19, '2017-11-24 01:02:27'),
	(216, NULL, 13, 19, '2017-11-24 01:02:27'),
	(217, NULL, 14, 19, '2017-11-24 01:02:27'),
	(218, NULL, 15, 19, '2017-11-24 01:02:27'),
	(219, NULL, 16, 19, '2017-11-24 01:02:27'),
	(220, NULL, 1, 20, '2017-11-24 01:02:27'),
	(221, NULL, 2, 20, '2017-11-24 01:02:27'),
	(222, NULL, 3, 20, '2017-11-24 01:02:27'),
	(223, NULL, 4, 20, '2017-11-24 01:02:27'),
	(224, NULL, 5, 20, '2017-11-24 01:02:27'),
	(225, NULL, 6, 20, '2017-11-24 01:02:27'),
	(226, NULL, 7, 20, '2017-11-24 01:02:27'),
	(227, NULL, 8, 20, '2017-11-24 01:02:27'),
	(228, NULL, 9, 20, '2017-11-24 01:02:27'),
	(229, NULL, 10, 20, '2017-11-24 01:02:27'),
	(230, NULL, 11, 20, '2017-11-24 01:02:27'),
	(231, NULL, 12, 20, '2017-11-24 01:02:27'),
	(232, NULL, 13, 20, '2017-11-24 01:02:27'),
	(233, NULL, 14, 20, '2017-11-24 01:02:27'),
	(234, NULL, 15, 20, '2017-11-24 01:02:27'),
	(235, NULL, 16, 20, '2017-11-24 01:02:27'),
	(236, NULL, 1, 21, '2017-11-24 01:04:31'),
	(237, NULL, 2, 21, '2017-11-24 01:04:31'),
	(238, NULL, 3, 21, '2017-11-24 01:04:31'),
	(239, NULL, 4, 21, '2017-11-24 01:04:31'),
	(240, NULL, 5, 21, '2017-11-24 01:04:31'),
	(241, NULL, 6, 21, '2017-11-24 01:04:31'),
	(242, NULL, 7, 21, '2017-11-24 01:04:31'),
	(243, NULL, 8, 21, '2017-11-24 01:04:31'),
	(244, NULL, 9, 21, '2017-11-24 01:04:31'),
	(245, NULL, 10, 21, '2017-11-24 01:04:31'),
	(246, NULL, 11, 21, '2017-11-24 01:04:31'),
	(247, NULL, 12, 21, '2017-11-24 01:04:31'),
	(248, NULL, 13, 21, '2017-11-24 01:04:31'),
	(249, NULL, 14, 21, '2017-11-24 01:04:31'),
	(250, NULL, 15, 21, '2017-11-24 01:04:31'),
	(251, NULL, 16, 21, '2017-11-24 01:04:31'),
	(252, NULL, 17, 21, '2017-11-24 01:04:31'),
	(253, NULL, 18, 21, '2017-11-24 01:04:31'),
	(254, NULL, 19, 21, '2017-11-24 01:04:32'),
	(255, NULL, 20, 21, '2017-11-24 01:04:32'),
	(256, NULL, 1, 22, '2017-11-24 01:04:32'),
	(257, NULL, 2, 22, '2017-11-24 01:04:32'),
	(258, NULL, 3, 22, '2017-11-24 01:04:32'),
	(259, NULL, 4, 22, '2017-11-24 01:04:32'),
	(260, NULL, 5, 22, '2017-11-24 01:04:32'),
	(261, NULL, 6, 22, '2017-11-24 01:04:32'),
	(262, NULL, 7, 22, '2017-11-24 01:04:32'),
	(263, NULL, 8, 22, '2017-11-24 01:04:32'),
	(264, NULL, 9, 22, '2017-11-24 01:04:32'),
	(265, NULL, 10, 22, '2017-11-24 01:04:32'),
	(266, NULL, 11, 22, '2017-11-24 01:04:32'),
	(267, NULL, 12, 22, '2017-11-24 01:04:32'),
	(268, NULL, 13, 22, '2017-11-24 01:04:32'),
	(269, NULL, 14, 22, '2017-11-24 01:04:32'),
	(270, NULL, 15, 22, '2017-11-24 01:04:32'),
	(271, NULL, 16, 22, '2017-11-24 01:04:32'),
	(272, NULL, 17, 22, '2017-11-24 01:04:32'),
	(273, NULL, 18, 22, '2017-11-24 01:04:32'),
	(274, NULL, 19, 22, '2017-11-24 01:04:32'),
	(275, NULL, 20, 22, '2017-11-24 01:04:32');
/*INDEX*/

CREATE INDEX idx_patient ON attendance(patient); 
CREATE INDEX idx_doctor_responsable ON attendance(doctor_responsible);
CREATE INDEX idx_hospital ON  attendance(hospital);
CREATE INDEX idx_itu ON  attendance(bed);
CREATE INDEX idx_admition_date_itu ON attendance(admission_date_itu);
