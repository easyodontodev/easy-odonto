<?php
	
	require_once "../database/attendance.php";
	
	$conn = new AttendanceDb();
	$historic = $conn->searchHistoric($_GET['id']);
	$json = json_encode($historic);
	echo $json;