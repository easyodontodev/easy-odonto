<?php

	require_once "../controller/attendance.php";
	require_once "../controller/patient.php";

	$paciente = new PatientController();
	$return = $paciente->add();

	$atendimento = new AttendanceController();
	$atendimento->add($return->id);

	