<?php
	
	require_once "../database/attendance.php";
	
	$conn = new AttendanceDb();
	$attendance = $conn->searchIdWithEvaluation($_GET['id']);
	$json = json_encode($attendance);
	echo $json;