

//SCRIPT CADASTRO PACIENTE

function bloqueio(el) {
      var display = document.getElementById(el).style.display;
        if(display == "none")
            document.getElementById(el).style.display = 'block';
        else
            document.getElementById(el).style.display = 'none';
    }
	
/////////////////////////////////
	
//SCRIPT EDITAR PACIENTE

$(document).ready(function(){
		$('#formEditPatient').on('submit',function(e){
			e.preventDefault();
			$.ajax({
				type: 'POST',
				dataType: 'html',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				url: "../request/editPatient.php",
				success: function (data) {
					console.log(data);
					var id = $('#paciente_id').val();
					$('#modalEditPatient').modal('close');
					$('#patient'+id).html(data);
					
				}
			});
		});
	});

  //////////////////////////////
  
  //SCRIPT BUSCA DE PACIENTE

$(document).ready(function() {
		    $('#pacienteDatatable').DataTable({
			"pageLength": 30,	
			"oLanguage":{
		    "sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_, Total de _TOTAL_ Pacientes",
		    "sInfoEmpty": "Mostrando 0 até 0, Total de 0 Pacientes",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum paciente encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": "Próximo",
		        "sPrevious": "Anterior",
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
			
			}}
			
			);
		});	
		function visualizarPaciente(id) { 
			$.ajax({ 
				type: 'get',
				dataType: 'html',
				url: "../request/viewPatient.php?id=" + id,
				success: function (data) { 
					$("#modalEditPatient").html(data); 
					$( "#modalEditPatient" ).modal('open');
				} 
			}); 
		}
		
/////////////////////////////////