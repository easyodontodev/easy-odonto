

//SCRIPT INICIALIZACAO DE FRAMEWORKS

(function($){
          $(function(){
              $('select').material_select();
              $('.button-collapse').sideNav();
              $('.parallax').parallax();
          });
      })(jQuery);
	  
/////////////////////////////////

//SCRIPT CABECALHO

	function expireSessoes(){
		$.ajax({
			type: 'GET',
			dataType: 'html',
			url: "../request/logout.php",
			success: function (data) {
				$('#modalReLogin').modal('open');
			}
		});
		
	}

/////////////////////////////////


//SCRIPT INICIALIZACAO DE DATEPICKER
$(document).ready(function() {
$('.datepicker').pickadate({
    	monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
  		monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
  		weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
  		weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
  		today: 'Hoje',
  		clear: 'Limpar',
  		close: 'Pronto',
  		labelMonthNext: 'Próximo mês',
  		labelMonthPrev: 'Mês anterior',
  		labelMonthSelect: 'Selecione um mês',
  		labelYearSelect: 'Selecione um ano',
        selectYears:100,
  		max:$.now(),
		formatSubmit: 'yyyy-mm-dd',
  		format: 'dd !de mmmm !de yyyy'
    });
    
      $('select').material_select();
    });
	
/////////////////////////////////
 
 
		
		
	

	