//SCRIPT EDITAR FUNCIONARIO

$(document).ready(function(){
		$('#formEditEmployee').on('submit',function(e){
			e.preventDefault();
			$.ajax({
				type: 'POST',
				dataType: 'html',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				url: "../request/editEmployee.php",
				success: function (data) {
					var id = $('#funcionario_id').val();
					$('#modalEditEmployee').modal('close');
					$('#employee'+id).html(data);
					
				}
			});
		});
	});
	
/////////////////////////////////

//SCRIPT BUSCA DE FUNCIONARIO

$(document).ready(function() {
		    $('#funcionarioDatatable').DataTable({
			"pageLength": 30,	
			"oLanguage":{
		    "sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_. Total de _TOTAL_ Funcionários",
		    "sInfoEmpty": "Mostrando 0 até 0. Total de 0 Funcionários",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum funcionário encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": "Próximo",
		        "sPrevious": "Anterior",
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
			}});
		});	
		function visualizarFuncionario(id) { 
			$.ajax({ 
				type: 'get',
				dataType: 'html',
				url: "../request/viewEmployee.php?id=" + id,
				beforeSend: function () { 
				}, 
				success: function (data) { 
					$("#modalEditEmployee").html(data); 
					$( "#modalEditEmployee" ).modal('open');
				} 
			}); 
		}
		
/////////////////////////////////