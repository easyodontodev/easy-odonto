//SCRIPT DE CONSULTAR ATENDIMENTO

$(onPageLoad);


function onPageLoad(){
	$( ".portlet" )
	.addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
	.find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" )
};
/////////////////////////////////

//SCRIPT INDICADORES

$(document).ready(function(){

	$('.modal').modal();
	$('#modal1').modal('open');
	$('#modal1').modal('close');
});

//SCRIP HISTORICO

$(document).ready(function(){
	$('.modal').modal();
	$('#modalHistorico').modal('open');
	$('#modalHistorico').modal('close');
});


//SCRIPT ATENDIMENTO

function addIndicadores(){
	console.log($('#formIndicadores').serialize());
	$.ajax({
		type: 'POST',
		data: $('#formIndicadores').serialize(),
		url: '../request/addindicadores.php',
		dataType: 'html',
		success: function(data) {
		}
	});
}

function editAttendanceSubmitConclude(nameForm){
	var form = $("#"+nameForm).serializeArray();
	console.log(form);
	$.ajax({
		type: 'POST',
		data: "id="+form[0].value+"&state="+form[1].value+"&description=",
		url: '../request/editAttendance.php',
		dataType: 'html',
		success: function(data) {
			if($('input:radio[name=stateAttendance]:checked').val() == 1){
				$("#avaliacao").append(data);
			}else if($('input:radio[name=stateAttendance]:checked').val() == 3){
				$("#aprovacao").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 2){
				$("#reavaliacao").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 13){
				$("#concluido").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 9){
				$("#acompanhamento").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 6){
				$("#orcamento").append(data);
			}
			var id = $('.attendanceId').val();
			$('#attendance'+id).remove();
			onPageLoad();
		}
	});
	$('#editAttendanceConcludeModal').modal('close');
};

function editAttendanceSubmit(nameForm){
	var array = $("#"+nameForm).serializeArray();
	var jsonString = "[";
	var attendanceId = 0;
	var stateAttendance = 0;
	for(var i=0;i<array.length;i++){
		if(i == (array.length-1) && array[i].name != "attendanceId" && array[i].name != "stateAttendance"){
			jsonString+="{\"name\":\""+array[i].name+"\"" + ",\"description\":\"" + array[i].value + "\"}";
		}else if(i != (array.length-1) && array[i].name != "attendanceId" && array[i].name != "stateAttendance"){
			jsonString+="{\"name\":\""+array[i].name+"\"" + ",\"description\":\"" + array[i].value + "\"},";
		}else if(array[i].name == "attendanceId"){
			attendanceId = array[i].value;
		}else if(array[i].name == "stateAttendance"){
			stateAttendance = array[i].value;
		}
	}
	jsonString += "]";
	$.ajax({
		type: 'POST',
		data: "id="+attendanceId+"&state="+stateAttendance+"&description="+jsonString,
		url: '../request/editAttendance.php',
		dataType: 'html',
		success: function(data) {
			if($('input:radio[name=stateAttendance]:checked').val() == 3){
				$("#aprovacao").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 2){
				$("#reavaliacao").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 13){
				$("#concluido").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 9){
				$("#acompanhamento").append(data);
			}else if ($('input:radio[name=stateAttendance]:checked').val() == 6){
				$("#orcamento").append(data);
			}
			var id = $('.attendanceId').val();
			$('#attendance'+id).remove();
			onPageLoad();
		}
	});
	$('#editAttendanceModal').modal('close');
};

function editAttendanceSubmitMonitoring(nameForm){
	var array = $("#"+nameForm).serializeArray();
	var jsonString = "[";
	var attendanceId = 0;
	var stateAttendance = 0;
	var arrayNames = [];
	var arrayValues = [];
	var arrayNumbers = [];
	for(var j=0;j<array.length;j++){
		if(array[j].name == "nome_procedimento"){
			arrayNames.push(array[j].value);
		}
		if(array[j].name == "date_procedimento"){
			arrayValues.push(array[j].value);
		}
		if(array[j].name == "number_procedimento"){
			arrayNumbers.push(array[j].value);
		}
	}
	for(var i=0;i<array.length;i++){
		if(array[i].name == "attendanceId"){
			attendanceId = array[i].value;
		}else if(array[i].name == "stateAttendance"){
			stateAttendance = array[i].value;
		}
	}
	if(stateAttendance == '9'){
		for(var o=0;o<arrayNames.length;o++){
			if(o == (arrayNames.length-1)){
				jsonString+="{\"number\":\""+arrayNumbers[o]+"\"" + ",\"description\":\"" + arrayNames[o] + "\",\"date\":\""+arrayValues[o]+"\"}";
			}else if(o != (arrayNames.length-1)){
				jsonString+="{\"number\":\""+arrayNumbers[o]+"\"" + ",\"description\":\"" + arrayNames[o] + "\",\"date\":\""+arrayValues[o]+"\"},";
			}
		}
		jsonString += "]";
		$.ajax({
			type: 'POST',
			data: "id="+attendanceId+"&state="+stateAttendance+"&description="+jsonString,
			url: '../request/editAttendance.php',
			dataType: 'html',
			success: function(data) {
				if($('input:radio[name=stateAttendance]:checked').val() == 3){
					$("#aprovacao").append(data);
				}else if ($('input:radio[name=stateAttendance]:checked').val() == 2){
					$("#reavaliacao").append(data);
				}else if ($('input:radio[name=stateAttendance]:checked').val() == 13){
					$("#concluido").append(data);
				}else if ($('input:radio[name=stateAttendance]:checked').val() == 6){
					$("#orcamento").append(data);
				}else if ($('input:radio[name=stateAttendance]:checked').val() == 9){
					$("#acompanhamento").append(data);
				}
				var id = $('.attendanceId').val();
				$('#attendance'+id).remove();
				onPageLoad();
			}
		});
		$('#editAttendanceModal').modal('close');
	}else{
		editAttendanceSubmit(nameForm);
	}
};


function addAttendanceModal(){
	$.ajax({
		type: 'POST',
		dataType: 'html',
		data:   'patient='+$('#patient').val()
		+	'&birthdaydate='+$('#birthdaydate').val()
		+	'&cpf='+$('#cpf').val()
		+	'&gender='+$('#gender').val()
		+	'&plano_de_saude='+$('#plano_de_saude').val()
		+	'&admission_cause='+$('#admission_cause').val()
		+	'&hospital='+$('#hospital').val()
		+	'&uti='+$('#uti').val()
		+	'&leito='+$('#leitoCad').val()
		+	'&user='+$('#user').val()
		+	'&Iduser='+$('#Iduser').val()
		+	'&doctor='+$('#doctor').val(),
		url: "../request/addAttendance.php",
		success: function (data) {
			$('#modalCadAttendance').modal('close');
			$("#avaliacao").append(data);
			$( ".portlet" )
			.addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
			.addClass( "ui-widget-header ui-corner-all" );
		}
	});

};

$(document).ready(function(){
	$('#btnAddPatientModal').click(function(){
		$('#modalCadPatient').modal('close');
		$('#modalCadAttendance').modal('open');
		$.ajax({
			type: 'POST',
			data: $('#formAddPatient').serialize(),
			url: '../request/addPatientModal.php',
			dataType: 'html',
			success: function(data) {
			}
		});
	});

	$("#btnAddPatient").click(function(){
		$.ajax({
			type: 'GET',
			url: 'cadastro_paciente_modal.php',
			dataType: 'html',
			success: function(data) {
				$('#modalCadAttendance').modal('close');
				$('#modalCadPatient').html(data);
				$('#modalCadPatient').modal('open');
			}
		});
	});



	$("#btnAddBackAttendance").click(function(){
		$.ajax({
			type: 'GET',
			url: 'atendimentos.php',
			dataType: 'html',
			success: function(data) {

				
				$('#modalCadPatient').modal('close');
				$('#modalCadAttendance').modal('open');
			}
		});
	});



	$('#botaoAddAttendance').click(function(){
		$('#Idpatient').val('');
		$('#patient').val('');
		$('#obsPaciente').val('');
		$('#admdate').val('');
		$('#Iduser').val('');
		$('#user').val('');
		$('#mostrapaciente').html('');
		$('#mostramedico').html('');
		$('#mostrapaciente').css('display','none');
		$('#mostramedico').css('display','none');
	});

	$("#botaoPesquisar").click(function(){
		var texto = $('#pesquisa').val();
		$("#boardAttendance .item-pesquisa").show();
		$("#boardAttendance .item-pesquisa").each(function(){
			if($('.nome-pesquisa', this).text().toUpperCase().indexOf(texto.toUpperCase()) < 0)
				$(this).hide();
		});
	});

	$('.modal').modal();
	$('.pacientes').click(function(){
		var text = $(this).text();
		$('#patient').val();
	});
});

function procurarLeito(value,nomeLeito){
	$.ajax({
		type: 'get',
		dataType: 'html',
		url: "../request/searchBed.php?id=" + value,
		success: function (data) {
			$('#'+nomeLeito).html(data);
			$('select').material_select();
		}
	});
}

function procurarUser() {
	value = $('#user').val();
	if(value != ""){
		$.ajax({
			type: 'get',
			dataType: 'html',
			url: "../request/searchEmployee.php?name=" + value,
			success: function (data) {
				$('#mostramedico').css('display','block');
				$('#mostramedico').html(data);
			}
		});
	}else{
		$('#mostramedico').css('display','none');
		$('#mostramedico').html("");
	}
}

function procurarPaciente() {
	value = $('#patient').val();
	if(value != ""){
		$.ajax({
			type: 'get',
			dataType: 'html',
			url: "../request/searchPatient.php?name=" + value,
			success: function (data) {
				$('#mostrapaciente').css('display','block');
				$('#mostrapaciente').html(data);
			}
		});
	}else{
		$('#mostrapaciente').css('display','none');
		$('#mostrapaciente').html("");
	}
}

function clickpaciente(text,id){
	$('#patient').val(text);
	$('#Idpatient').val(id);
	$('#mostrapaciente').css('display','none');
};

function clickmedico(text,id){
	$('#user').val(text);
	$('#Iduser').val(id);
	$('#mostramedico').css('display','none');
};

$(document).ready(function(){
	$('.modal').modal();
});

$('#modalCadAttendance').modal('open');
$('#modalCadAttendance').modal('close');

/////////////////////////////////

//SCRIPT EDITAR ATENDIMENTO

function checkAprovationReavaliation(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkConcludeReavaliation').html('');
	$('#checkAprovationReavaliation').html(
		"<p>" +
		"  <label>Observações</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceReavaliation')\" name=\"observacoes\" id=\"observacoes\"></textarea>" +
		"</p>"
		);
}

function checkAprovation(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkReavaliation').html('');
	$('#checkConclude').html('');
	$('#checkAprovation').html(
		"<p>" +
		"  <label>Avaliação inicial odontológica</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textAvaliacaoInicial\" id=\"textAvaliacaoInicial\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Histórico odontológico</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textHistorico\" id=\"textHistorico\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Histórico médico</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textHistoricoMed\" id=\"textHistoricoMed\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Motivo da internação</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textMotivoInternacao\" id=\"textMotivoInternacao\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Conduta</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textConduta\" id=\"textConduta\"></textarea>" +
		"</p>"+
		"<p>" +
		"  <label>Observações</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"observacoes\" id=\"observacoes\"></textarea>" +
		"</p>"
		);
}


function checkReavaliation(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkAprovation').html('');
	$('#checkConclude').html('');
	$('#checkReavaliation').html(
		"<p>" +
		"  <label>Avaliação inicial odontológica</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textAvaliacaoInicial\" id=\"textAvaliacaoInicial\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Histórico odontológico</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textHistorico\" id=\"textHistorico\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Histórico médico</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textHistoricoMed\" id=\"textHistoricoMed\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Motivo da internação</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textMotivoInternacao\" id=\"textMotivoInternacao\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Conduta</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textConduta\" id=\"textConduta\"></textarea>" +
		"</p>"+
		"<p>" +
		"  <label>Observações</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"observacoes\" id=\"observacoes\"></textarea>" +
		"</p>");
}


function checkConlcudeModal(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkConcludeAprovation').html('');
	$('#checkOrcamento').html('');
}

function checkOrcamento(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkConcludeAprovation').html('');
	$('#checkOrcamento').html(
		"<p>"+
		"<input type=\"checkbox\" id=\"aprovadoFamilia\" onchange=\"valideModalEditAttendanceAprovado()\" name=\"aprovadoFamilia\">"+
		"<label for=\"aprovadoFamilia\">Aprovado pela família</label>"+
		"</p>"+
		"<p>"+
		"<input type=\"checkbox\" id=\"aprovadoMedico\" onchange=\"valideModalEditAttendanceAprovado()\" name=\"aprovadoMedico\">"+
		"<label for=\"aprovadoMedico\">Aprovado pelo médico responsável</label>"+
		"</p>"+
		"<p>"+
		"<label>Observações</label><br>"+
		"<textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendanceAprovado()\" name=\"observacoes\" id=\"observacoesAprovation\"></textarea>"+
		"</p>");
}


function valideModalEditAttendance(nameForm){
	var valida = true;
	var array = $('#'+nameForm).serializeArray();
	for(var i=0;i<array.length;i++){
		if(array[i].value == ''){
			valida = false;
		}
	}

	if(!valida){
		$('.buttonEditAttendance').attr( "disabled", "disabled" );
	}else{
		$('.buttonEditAttendance').removeAttr( "disabled");
	}

};


function valideModalEditAttendanceAprovado(){
	var valida = true;
	if($('#observacoesAprovation').val() == ''){
		valida = false;
	}
	if(!$('#aprovadoFamilia').is(':checked')){
		valida = false;
	}
	if(!$('#aprovadoMedico').is(':checked')){
		valida = false;
	}
	if(!valida){
		$('.buttonEditAttendance').attr( "disabled", "disabled" );
	}else{
		$('.buttonEditAttendance').removeAttr( "disabled");
	}
};


function openModalEditAttendance(id){
	$('.attendanceId').val(id);
	returnAttedanceForModal(id);
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('.nome-pesquisa-modal').html($('#attendance'+id+' .nome-pesquisa').html());
	$('.doctor-responsavel-modal').html($('#attendance'+id+' .doctor-responsavel').html());
	$('.nme-uti-modal').html($('#attendance'+id+' .nme-uti').html());
	$('.nme-leito-modal').html($('#attendance'+id+' .nme-leito').html());
	$('#editAttendanceModal').modal('open');
}

function returnHistoric(id){
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: "../request/returnHistoric.php?id=" + id,
		success: function (data) {
			var json = JSON.parse(data);
			console.log(json);
		}
	});
}

function returnAttedanceForModal(id){
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: "../request/returnAttendance.php?id=" + id,
		success: function (data) {
			var json = JSON.parse(data);
			if(json.id != null){
				checkedEvaluation(json.headbord,"cabeceira");
				checkedEvaluation(json.diet,"dieta");
				checkedEvaluation(json.conscious_oriented,"hope");
				checkedEvaluation(json.ventilation,"ventilacao");
				checkedEvaluation(json.odt,"odontologia");
				checkedEvaluation(json.iho_pc,"fatores_odonto");
				checkedEvaluation(json.mon_odt,"nota");
				$('#formIndicadores input[name="fatores_odonto"]').val(json.iho_pc);
				$.each($('#fatores_odonto'), function( index, input ) {
					input.value = json.iho_pc;
					
					(function($){
						$(function(){
							$('select').material_select();
							$('.button-collapse').sideNav();
							$('.parallax').parallax();
						});
					})(jQuery);
				});
				readOnly('simCabeceira');
				readOnly('naoCabeceira');
				readOnly('oral');
				readOnly('nao-oral');
				readOnly('mista');
				readOnly('IHOPC');
				readOnly('efetiva');
				readOnly('deficiente');
				readOnly('precaria');
				readOnly('ambiente');
				readOnly('IOT');
				readOnly('TQT');
				readOnly('outros');
				readOnly('acompOdonto');
				readOnly('HO');
				readOnly('atendOdonto');
				readOnly('ho_satisfatorio');
				readOnly('ho_deficiente');
				readOnly('ho_precaria');
				$('.addIndicadores').css("display","none");
			}else{
				uncheckedEvaluation('simCabeceira');
				uncheckedEvaluation('naoCabeceira');
				uncheckedEvaluation('oral');
				uncheckedEvaluation('nao-oral');
				uncheckedEvaluation('mista');
				uncheckedEvaluation('IHOPC');
				uncheckedEvaluation('efetiva');
				uncheckedEvaluation('deficiente');
				uncheckedEvaluation('precaria');
				uncheckedEvaluation('ambiente');
				uncheckedEvaluation('IOT');
				uncheckedEvaluation('TQT');
				uncheckedEvaluation('outros');
				uncheckedEvaluation('acompOdonto');
				uncheckedEvaluation('HO');
				uncheckedEvaluation('atendOdonto');
				uncheckedEvaluation('ho_satisfatorio');
				uncheckedEvaluation('ho_deficiente');
				uncheckedEvaluation('ho_precaria');
				$.each($('#fatores_odonto'), function( index, input ) {
					input.value = 'n';
					(function($){
						$(function(){
							$('select').material_select();
							$('.button-collapse').sideNav();
							$('.parallax').parallax();
						});
					})(jQuery);
				});
				$('.addIndicadores').css("display","block");
			}
			
			$('.nome-pesquisa-modal').html(json.namePatient);
			$('.doctor-responsavel-modal').html(json.nameDoctor);
			$('#leitoLabel').html(json.leito);
			$.each($('#uti'), function( index, input ) {
				input.value = json.uti;

				(function($){
					$(function(){
						$('select').material_select();
						$('.button-collapse').sideNav();
						$('.parallax').parallax();
					});
				})(jQuery);
			});
			$.each($('#leito'), function( index, input ) {
				input.value = json.leito;

				(function($){
					$(function(){
						$('select').material_select();
						$('.button-collapse').sideNav();
						$('.parallax').parallax();
					});
				})(jQuery);
			});
		}
	});
	returnHistoric(id);
}

function checkedEvaluation(json,name){
	$.each($('#formIndicadores input[name="'+name+'"]'), function( index, input ) {
		if(input.value == json){
			input.checked = true;
		}
	});
}

function readOnly(name){
	$('#'+name+'').attr("disabled",true);
}

function uncheckedEvaluation(name){
	$.each($('#'+name+''), function( index, input ) {
		input.checked = false;
	});
}

function openModalEditAttendanceReavaliation(id){
	$('.attendanceId').val(id);
	returnAttedanceForModal(id);
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('.nome-pesquisa-modal').html($('#attendance'+id+' .nome-pesquisa').html());
	$('.doctor-responsavel-modal').html($('#attendance'+id+' .doctor-responsavel').html());
	$('.nme-uti-modal').html($('#attendance'+id+' .nme-uti').html());
	$('.nme-leito-modal').html($('#attendance'+id+' .nme-leito').html());
	$('#editAttendanceReavaliationModal').modal('open');
}


function openModalEditAttendanceAprovation(id){
	$('.attendanceId').val(id);
	returnAttedanceForModal(id);
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('.nome-pesquisa-modal').html($('#attendance'+id+' .nome-pesquisa').html());
	$('.doctor-responsavel-modal').html($('#attendance'+id+' .doctor-responsavel').html());
	$('.nme-uti-modal').html($('#attendance'+id+' .nme-uti').html());
	$('.nme-leito-modal').html($('#attendance'+id+' .nme-leito').html());
	$('#editAttendanceAprovationModal').modal('open');
}

function openModalEditAttendanceConclude(id){
	$('.attendanceId').val(id);
	returnAttedanceForModal(id);
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('.nome-pesquisa-modal').html($('#attendance'+id+' .nome-pesquisa').html());
	$('.doctor-responsavel-modal').html($('#attendance'+id+' .doctor-responsavel').html());
	$('.nme-uti-modal').html($('#attendance'+id+' .nme-uti').html());
	$('.nme-leito-modal').html($('#attendance'+id+' .nme-leito').html());
	$('#editAttendanceConcludeModal').modal('open');
}
function openModalEditAttendanceBudget(id){
	$('.attendanceId').val(id);
	returnAttedanceForModal(id);
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('.nome-pesquisa-modal').html($('#attendance'+id+' .nome-pesquisa').html());
	$('.doctor-responsavel-modal').html($('#attendance'+id+' .doctor-responsavel').html());
	$('.nme-uti-modal').html($('#attendance'+id+' .nme-uti').html());
	$('.nme-leito-modal').html($('#attendance'+id+' .nme-leito').html());
	$.ajax({
		type: 'GET',
		url: '../request/searchMonitoring.php?id='+id,
		dataType: 'html',
		success: function(data) {
			var json = JSON.parse(data);
			$('#monitorings').html('');
			for(var i = 0;i<json.length;i++){
				$('#monitorings').append(
					'<div id="monitoring'+json[i].number+'">'+
					'<input name="number_procedimento" id="number_procedimento" type="hidden"  value="'+json[i].number+'" required>'+
					'<div style="width:40%;float:left;margin-right:5%">'+
					'<label for="nome_procedimento">Descrição de atendimento</label>'+
					'<input name="nome_procedimento" oninput="saveMonitoring('+id+')" id="nome_procedimento" type="text" value="'+json[i].name+'" required>'+
					'</div>'+
					'<div style="width:40%;float:left;margin-right:5%">'+
					'<label for="date_procedimento">Data de atendimento</label>'+
					'<input name="date_procedimento" onchange="saveMonitoring('+id+')" id="date_procedimento" type="datetime-local" value="'+json[i].date+'" required>'+
					'</div>'+
					'<a id="delMonitoring" style="margin-top:20px;margin-bottom:20px" onclick="deleteMonitoring('+json[i].number+','+id+')" class="btn-floating red positionStatic"> <i class="material-icons">delete</i></a>'+
					'</div>'
					);
				$('.datetime').mask('99/99/9999 99:99:99');
			}
		}
	});
	$('#editAttendanceBudgetModal').modal('open');
}
function openModalEditAttendanceMonitoring(id){
	$('.attendanceId').val(id);
	returnAttedanceForModal(id);
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('.nome-pesquisa-modal').html($('#attendance'+id+' .nome-pesquisa').html());
	$('.doctor-responsavel-modal').html($('#attendance'+id+' .doctor-responsavel').html());
	$('.nme-uti-modal').html($('#attendance'+id+' .nme-uti').html());
	$('.nme-leito-modal').html($('#attendance'+id+' .nme-leito').html());
	$.ajax({
		type: 'GET',
		url: '../request/searchMonitoring.php?id='+id,
		dataType: 'html',
		success: function(data) {
			var json = JSON.parse(data);
			$('#acompanhamentos').html('');
			for(var i = 0;i<json.length;i++){
				$('#acompanhamentos').append(
					'<div id="monitoring'+json[i].number+'">'+
					'<div style="width:40%;float:left;margin-right:5%">'+
					'<label for="nome_procedimento">Descrição de atendimento</label>'+
					'<label>'+json[i].name+'</label>'+
					'</div>'+
					'<div style="width:40%;float:left;margin-right:5%">'+
					'<label>Data de atendimento</label>'+
					'<label>'+json[i].date+'</label>'+
					'</div>'+
					'</div>'
					);
				$('.datetime').mask('99/99/9999 99:99:99');
			}
		}
	});
	$('#editAttendanceMonitoringModal').modal('open');
}


function addMonitoring(){
	var numberValide = false;
	var number = 1;
	var array = $("#formEditAttendanceMonitoring").serializeArray();
	while(!numberValide){
		number++;
		var repeat = false;
		for(var j=0;j<array.length;j++){
			if(array[j].name == "number_procedimento"){
				if(number == array[j].value){
					repeat = true;
				}
			}
		}
		if(!repeat){
			numberValide = true;
		}
	}
	var id = $('.attendanceId').val();
	$('#monitorings').append(
		'<div id="monitoring'+number+'">'+
		'<input name="number_procedimento" id="number_procedimento" type="hidden"  value="'+number+'" required>'+
		'<div style="width:40%;float:left;margin-right:5%">'+
		'<label for="nome_procedimento">Descrição de atendimento</label>'+
		'<input name="nome_procedimento" oninput="saveMonitoring('+id+')" id="nome_procedimento" type="text" value="" required>'+
		'</div>'+
		'<div style="width:40%;float:left;margin-right:5%">'+
		'<label for="date_procedimento">Data de atendimento</label>'+
		'<input name="date_procedimento" onchange="saveMonitoring('+id+')" id="date_procedimento" type="datetime-local" value="" required>'+
		'</div>'+
		'<a id="delMonitoring" style="margin-top:20px;margin-bottom:20px" onclick="deleteMonitoring('+number+','+id+')" class="btn-floating red positionStatic"> <i class="material-icons">delete</i></a>'+
		'</div>'
		);
	saveMonitoring(id);
};


function deleteMonitoring(number,id){
	$('#monitoring'+number).remove();
	saveMonitoring(id);
};

function saveMonitoring(id){
	var array = $("#formEditAttendanceMonitoring").serializeArray();
	var jsonString = "[";
	var attendanceId = id;
	var arrayNames = [];
	var arrayDates = [];
	var arrayNumbers = [];
	for(var j=0;j<array.length;j++){
		if(array[j].name == "nome_procedimento"){
			arrayNames.push(array[j].value);
		}
		if(array[j].name == "date_procedimento"){
			arrayDates.push(array[j].value);
		}
		if(array[j].name == "number_procedimento"){
			arrayNumbers.push(array[j].value);
		}
	}
	for(var o=0;o<arrayNames.length;o++){
		if(o == (arrayNames.length-1)){
			jsonString+="{\"number\":\""+arrayNumbers[o]+"\"" + ",\"name\":\"" + arrayNames[o] + "\",\"date\":\""+arrayDates[o]+"\"}";
		}else if(o != (arrayNames.length-1)){
			jsonString+="{\"number\":\""+arrayNumbers[o]+"\"" + ",\"name\":\"" + arrayNames[o] + "\",\"date\":\""+arrayDates[o]+"\"},";
		}
	}
	jsonString += "]";
	$.ajax({
		type: 'POST',
		data: "id="+attendanceId+"&description="+jsonString,
		url: '../request/saveMonitoring.php',
		dataType: 'html',
		success: function(data) {
		}
	});
}


function checkConclude(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkAprovation').html('');
	$('#checkReavaliation').html('');
	$('#checkConclude').html(
		"<p>" +
		"  <label>Avaliação inicial odontológica</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textAvaliacaoInicial\" id=\"textAvaliacaoInicial\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Histórico odontológico</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textHistorico\" id=\"textHistorico\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Histórico médico</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textHistoricoMed\" id=\"textHistoricoMed\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Motivo da internação</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textMotivoInternacao\" id=\"textMotivoInternacao\"></textarea>" +
		"</p>" +
		"<p>" +
		"  <label>Conduta</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"textConduta\" id=\"textConduta\"></textarea>" +
		"</p>"+
		"<p>" +
		"  <label>Observações</label><br>" +
		"  <textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideModalEditAttendance('formEditAttendanceAvaliation')\" name=\"observacoes\" id=\"observacoes\"></textarea>" +
		"</p>"
		);  
	$('select').material_select();
}


function checkConcludeReavaliation(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkAprovationReavaliation').html('');
	$('#checkConcludeReavaliation').html(
		"<div class=\"input-field col s12\">" +
		"    <select id='selectObito' name=\"stateComplete\" onchange='valideSelectEditAttendance()'>" +
		"      <option value=\"\" disabled selected>Concluído</option>" +
		"      <option value=\"1\">Alta</option>" +
		"      <option value=\"2\">Óbito</option>" +
		"      <option value=\"3\">Não necessita de atendimento</option>" +
		"    </select>" +
		"  </div>"+
		"<p>"+
		"<label>Observações</label><br>"+
		"<textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideSelectEditAttendance()\" name=\"observacoes\" id=\"observacoesConcluido\"></textarea>"+
		"</p>"
		);  
	$('select').material_select();
}

function checkAcompanhamento(){
	var id = $('.attendanceId').val();
	$('#checkAcompanhamento').html(
					'<div id="monitoring'+json[i].number+'">'+
					'<div style="width:40%;float:left;margin-right:5%">'+
					'<label for="nome_procedimento">Descrição de atendimento</label>'+
					'<label>'+json[i].name+'</label>'+
					'</div>'+
					'<div style="width:40%;float:left;margin-right:5%">'+
					'<label>Data de atendimento</label>'+
					'<label>'+json[i].date+'</label>'+
					'</div>'+
					'</div>'
					);
	$('#checkConcludeAcompanhamento').html('');  
	$('select').material_select();
}

function checkMonitoring(){
	var id = $('.attendanceId').val();
	$('#checkMonitoring').html('<div id="monitorings">'+
					'<div id="monitoring1">'+
						'<input name="number_procedimento" id="number_procedimento" type="hidden"  value="1" required>'+
						'<div style="width:40%;float:left;margin-right:5%">'+
							'<label for="nome_procedimento">Descrição de atendimento</label>'+
							'<input name="nome_procedimento" oninput="saveMonitoring('+id+');valideModalAddAttendanceOrcamento()" id="nome_procedimento" type="text" value="" required>'+
						'</div>'+
						'<div style="width:40%;float:left;margin-right:5%">'+
							'<label for="date_procedimento">Data de atendimento</label>'+
							'<input name="date_procedimento" onchange="saveMonitoring('+id+');valideModalAddAttendanceOrcamento()" id="date_procedimento" type="datetime-local" value="" required>'+
						'</div>'+
						'<a id="delMonitoring" style="margin-top:20px;margin-bottom:20px" onclick="deleteMonitoring(1,'+id+')" class="btn-floating red positionStatic"> <i class="material-icons">delete</i></a>'+
					'</div>'+
				'</div>'+
				'<a id="addMonitoring" onclick="addMonitoring()" style="float:left" class="btn-floating blue positionStatic"> <i class="material-icons">add</i></a>'+
				'<label>Observações</label><br>'+
				  '<textarea rows="3" style="resize:none;overflow-y:auto;height:65px;" oninput="valideModalEditAttendanceAprovado()" name="observacoes" id="observacoesAprovation"></textarea>'+
				'</p>'+
				'<div id="checkMonitoring">'+
				'<div id="monitorings">');
	$('#checkConcludeMonitoring').html('');  
	$('select').material_select();
}

function checkConcludeMonitoring(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkMonitoring').html('');
	$('#checkConcludeMonitoring').html(
		"<div class=\"input-field col s12\">" +
		"    <select id='selectObito' name=\"stateComplete\" onchange='valideSelectEditAttendance()'>" +
		"      <option value=\"\" disabled selected>Concluído</option>" +
		"      <option value=\"1\">Alta</option>" +
		"      <option value=\"2\">Óbito</option>" +
		"      <option value=\"3\">Não necessita de atendimento</option>" +
		"    </select>" +
		"  </div>"+
		"<p>"+
		"<label>Observações</label><br>"+
		"<textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideSelectEditAttendance()\" name=\"observacoes\" id=\"observacoesConcluido\"></textarea>"+
		"</p>"
		);  
	$('select').material_select();
}

function checkConcludeAcompanhamento(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkAcompanhamento').html('');
	$('#checkConcludeAcompanhamento').html(
		"<div class=\"input-field col s12\">" +
		"    <select id='selectObito' name=\"stateComplete\" onchange='valideSelectEditAttendance()'>" +
		"      <option value=\"\" disabled selected>Concluído</option>" +
		"      <option value=\"1\">Alta</option>" +
		"      <option value=\"2\">Óbito</option>" +
		"      <option value=\"3\">Não necessita de atendimento</option>" +
		"    </select>" +
		"  </div>"+
		"<p>"+
		"<label>Observações</label><br>"+
		"<textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideSelectEditAttendance()\" name=\"observacoes\" id=\"observacoesConcluido\"></textarea>"+
		"</p>"
		);  
	$('select').material_select();
}

function checkConcludeAprovation(){
	$('.buttonEditAttendance').attr( "disabled", "disabled" );
	$('#checkOrcamento').html('');
	$('#checkConcludeAprovation').html(
		"<div class=\"input-field col s12\">" +
		"    <select id='selectObito' name=\"stateComplete\" onchange='valideSelectEditAttendance()'>" +
		"      <option value=\"\" disabled selected>Concluído</option>" +
		"      <option value=\"1\">Alta</option>" +
		"      <option value=\"2\">Óbito</option>" +
		"      <option value=\"3\">Não necessita de atendimento</option>" +
		"    </select>" +
		"  </div>"+
		"<p>"+
		"<label>Observações</label><br>"+
		"<textarea rows=\"3\" style=\"resize:none;overflow-y:auto;height:65px;\" oninput=\"valideSelectEditAttendance()\" name=\"observacoes\" id=\"observacoesConcluido\"></textarea>"+
		"</p>"
		);  
	$('select').material_select();
}

function valideSelectEditAttendance(){
	var valida = true;
	if($('#selectObito').val() == ''){
		valida = false;
	}
	if(!valida){
		$('.buttonEditAttendance').attr( "disabled", "disabled" );
	}else{
		$('.buttonEditAttendance').removeAttr( "disabled");
	}
};


function valideModalAddAttendance(){
	var valida = true;
	if($('#patient').val() == ''){
		valida = false;
	}
	if($('#cpf').val() == ''){
		valida = false;
	}
	if($('#birthdaydate').val() == ''){
		valida = false;
	}
	if($('#uti').val() == 'n'){
		valida = false;
	}
	if($('#leito').val() == 'n'){
		valida = false;
	}
	if($('#user').val() == ''){
		valida = false;
	}
	if(!valida){
		$('#adicionar').attr( "disabled", "disabled" );
	}else{
		$('#adicionar').removeAttr( "disabled");
	}
}

function valideModalAddAttendanceOrcamento(){
	var valida = true;
	if($('#nome_procedimento').val() == ''){
		valida = false;
	}
	if($('#date_procedimento').val() == ''){
		valida = false;
	}
	if(!valida){
		$('#adicionarOrcamento').attr( "disabled", "disabled" );
	}else{
		$('#adicionarOrcamento').removeAttr( "disabled");
	}
}

function valideModalAddIndicadores(){
	var valida = true;
	

	 if($('input:radio[name=cabeceira]:checked').val() === undefined){
		valida = false;

	}
	if($('input:radio[name=dieta]:checked').val() === undefined){
		valida = false;

	}
	if($('input:radio[name=hope]:checked').val() === undefined){
		valida = false;

	}
		if($('input:radio[name=ventilacao]:checked').val() === undefined){
		valida = false;

	}
		if($('input:radio[name=odontologia]:checked').val() === undefined){
		valida = false;

	}
		if($('input:radio[name=nota]:checked').val() === undefined){
		valida = false;

	}
		if($('#fatores_odonto').val().length == 0){
		valida = false;

	}
	if(!valida){
		$('#indicadoresbtn').attr( "disabled", "disabled" );
	}else{
		$('#indicadoresbtn').removeAttr( "disabled");
	}
}

function mudarAbas(opcao){
		if(opcao == 1){
			$('.conteudoPrincipal').css("display","inherit");
			$('.conteudoIndicadores').css("display","none");
			$('.conteudoHistorico').css("display","none");
		}else if(opcao == 2){
			$('.conteudoPrincipal').css("display","none");
			$('.conteudoIndicadores').css("display","inherit");
			$('.conteudoHistorico').css("display","none");
		}else if(opcao == 3){
			$('.conteudoPrincipal').css("display","none");
			$('.conteudoIndicadores').css("display","none");
			$('.conteudoHistorico').css("display","inherit");
		}
}

