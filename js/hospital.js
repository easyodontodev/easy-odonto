//SCRIPT CADASTRO HOSPITAL

$(document).ready(function(){
	  var index = 0;
  	$("#addUTI").click(function(){
  		$(".utis").append('<div class="uti"><div class="input-field col s5">'
        +'<i class="material-icons prefix">business</i>'
        + '<input name="nome_uti[]" id="nome_uti" title="Digite o nome da UTI" type="text" class="validate" required>'
        + '<label for="nome_uti">Nome da UTI</label>'
        + '</div>'
        + '<div class="input-field col s5">'
        + '<i class="material-icons prefix">business</i>'
        + ' <input name="quantidade_leito_uti[]" id="quantidade_leito_uti" title="Digite quantidade de leito da UTI" type="text" class="validate" required>'
        + '<label for="quantidade_leito_uti">Quantidade de leito da UTI</label>'
        +'</div>'
  	    +'</div></div>');
  	});
	$("#delUTI").click(function(){
  		$(".utis .uti:last").remove();
  	});
  });
	
/////////////////////////////////


//SCRIPT EDITAR E EXCLUIR HOSPITAL

	function buscarHospital(id){
		$.ajax({
			type: 'get',
			dataType: 'html',
			url: "../request/searchHospital.php?id=" + id,
			success: function (data) {
			  var dataJson = $.parseJSON(data);
			  $('#modalEditHospital #nome_hospital').val('');
			  $('#modalEditHospital #id_hospital').val('');
			  $('#modalEditHospital #telefone_uti').val('');
			  $('#modalEditHospital #telefone_chefe_uti').val('');
			  $('#modalEditHospital #nome_chefe_uti').val('');
			  $('#modalEditHospital #file_path').val('');
			  $('#modalEditHospital #image_path').val('');
			  $('#modalEditHospital #image_path_hidden').val('');
			  $('#modalEditHospital #nome_hospital').focusin();
			  $('#modalEditHospital #telefone_uti').focusin();
			  $('#modalEditHospital #telefone_chefe_uti').focusin();
			  $('#modalEditHospital #nome_chefe_uti').focusin();
			  $('#modalEditHospital #nome_hospital').val(dataJson['name']);
			  $('#modalEditHospital #id_hospital').val(dataJson['id']);
			  $('#modalEditHospital #telefone_uti').val(dataJson['telephone_uti']);
			  $('#modalEditHospital #telefone_chefe_uti').val(dataJson['telephone_chefe_uti']);
			  $('#modalEditHospital #nome_chefe_uti').val(dataJson['nome_chefe_uti']);
			  $('#modalEditHospital #image_path').val(dataJson['image']);
			  $('#modalEditHospital #image_path_hidden').val(dataJson['image']);
			}
		});
	}
	
		function buscarHospitalName(id){
			$.ajax({
				type: 'get',
				dataType: 'html',
				url: "../request/searchHospitalName.php?id=" + id,
				success: function (data) {
				  var dataJson = $.parseJSON(data);
				  $('#lblDeleteHospital').html('Deseja Realmente excluir o hospital '+dataJson['name']+'?');
				  $('#idHospitalExcluir').val(dataJson['id']);
				}
			});
		}
	$(document).ready(function(){
		$('#formEditHospital').on('submit',function(e){
			e.preventDefault();
			$.ajax({
				type: 'POST',
				dataType: 'html',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				url: "../request/editHospital.php",
				success: function (data) {
					$('#boardHospitais').html(data);
					$('#modalEditHospital').modal('close');
				}
			});
		});
		$('#yesDeleteHospital').click(function(){
			var id = $('#idHospitalExcluir').val();
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: "../request/deleteHospital.php?id="+id,
				success: function (data) {
					$('#boardHospitais').html(data);
				}
			});
		});
	});

	
/////////////////////////////////