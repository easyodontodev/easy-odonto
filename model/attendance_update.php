<?php
	
	class AttendanceUpdate{
		
		private $attendanceId;
		private $state;
		private $description;
		
		function __construct(){
		}
		
		public function getAttendanceId() {
			return $this->attendanceId;
		}
		
		public function setAttendanceId($attendanceId) {
			$this->attendanceId = $attendanceId;
		}
		
		public function getState() {
			return $this->state;
		}
		
		public function setState($state) {
			$this->state = $state;
		}
		
		public function getDescription() {
			return $this->description;
		}
		
		public function setDescription($description) {
			$this->description = $description;
		}
		
	
		
		
	}		