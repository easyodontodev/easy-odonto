<?php


	class indicadores

	{
		private $cabeceira;
		private $dieta;
		private $hope;
		private $ventilacao;
		private $odontologia;
		private $fatoresOdonto;
		private $nota;
		private $patient;


		public function __construct(){
			//gets e sets
		}

		public function getPatient(){
			return $this->patient;
		}
		
		public function setPatient($patient){
			$this->patient = $patient;
		}
		
		public function getCabeceira(){
			return $this->cabeceira;
		}
		
		public function setCabeceira($cabeceira){
			$this->cabeceira = $cabeceira;
		}
		
		public function getDieta(){
			return $this->dieta;
		}
		
		public function setDieta($dieta){
			$this->dieta = $dieta;
		}
		
		public function getHope(){
			return $this->hope;
		}
		
		public function setHope($hope){
			$this->hope = $hope;
		}
		
		public function getVentilacao(){
			return $this->ventilacao;
		}
		
		public function setVentilacao($ventilacao){
			$this->ventilacao = $ventilacao;
		}
		
		public function getOdontologia(){
			return $this->odontologia;
		}
		
		public function setOdontologia($odontologia){
			$this->odontologia = $odontologia;
		}
		
		public function getFatoresOdonto(){
			return $this->fatoresOdonto;
		}
		
		public function setFatoresOdonto($fatoresOdonto){
			$this->fatoresOdonto = $fatoresOdonto;
		}
		
		public function getNota(){
			return $this->nota;
		}
		
		public function setNota($nota){
			$this->nota = $nota;
		}
	}