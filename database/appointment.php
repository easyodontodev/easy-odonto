<?php


class Appointment {

	private $appointments = Array(
		//1 - EM AVALIAÇÃO


		//2 - EM REAVALIAÇÃO
		2 => Array("avaIniOdo","avaIntOraIni","hisOdo","hisMed","motInt","antPes","con"),
		
		//3 - EM APROVAÇÃO
		3 => Array("avaIniOdo","avaIntOraIni","hisOdo","hisMed","motInt","antPes","con","obs"),
		
		//4 - APROVADO PELO MÉDICO RESPONSÁVEL


		//5 - APROVADO PELA FAMÍLIA


		//6 - EM ORÇAMENTO
		6 => Array("aprPelFam","aprPelDenRes","obs"),

		//7 - ORÇAMENTO APROVADO PELA FMÍLIA


		//8 - ANEXAR PARECER


		//9 - EM ACOMPANHAMENTO
		9 => Array("pro"),

		//10 - DISPENSA


		//11 - ÓBITO


		//12 - ALTA


		//13 - CONCLUÍDO
		13 => Array("mot","obs"));


	public function insertStruct($status, $dados, $attendance) {

		$struct = $this->appointments[$status];
		foreach ($dados as $key => $value) {
			if(!in_array($key,$struct))		
				return "Deu erro";	
		}

		$json = html_entity_decode(json_encode(array_map('htmlentities',$dados)));

		try{
				$sql =" INSERT INTO appointment (description,status,attendance)
										VALUES ('$json','$status','$attendance')";


				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$result = $stmt->execute();

				return "Inserido com sucesso";
			}
			
			catch(PDOExeption $e){
				return $result;
			}

	}
	
	public function searchAppointmentId($idApp){

       $sql = " SELECT * FROM appointment a 
                WHERE a.attendance = :idApp 
                ORDER BY appointment.time DESC ";
       $conn = new DbConnector();
       $stmt = $conn->getConn()->prepare($sql);
       $stmt->execute();
       $result = $stmt->fetchAll(PDO::FETCH_OBJ);

       return $result;
    }

}