<?php 

	require_once "conection.php";

class DataDb {
	
	public function adicionarArquivo($id,$entidade,$nome,$uri,$tipo){
		$tipoId = $this->procurarTipoDocumento($tipo);
		
		try{
            $sql = "INSERT INTO documents(name,URI,doc_type) VALUES (:name,:uri,:tipo)";

        	$conn = new DbConnector();
            $stmt = $conn->getConn()->prepare($sql);
        	$stmt->bindParam(':name', $nome);
			$stmt->bindParam(':uri', $uri);
			$stmt->bindParam(':tipo', $tipoId);
    		$result = $stmt->execute();
			
			if($result){
				$documento = $this->retornarUltimoDocumento();
				$entidadeId = $this->procurarEntidade($entidade);
				$this->adicionarEntidadeArquivo($documento,$entidadeId,$id);
			}
			return $result;
		  
		}catch(PDOException $e)
		{
			var_dump($e);
			return $result;
		}
	
	}
	
	private function procurarEntidade($entidade){
		try{
             $sql = "SELECT id FROM meta_entity WHERE name = :entidade";

        	$conn = new DbConnector();
            $stmt = $conn->getConn()->prepare($sql);
        	$stmt->bindParam(':entidade', $entidade);
    		$stmt->execute(); 
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			return $result->id;
		  
		}catch(PDOException $e)
		{
			var_dump($e);
			return $result;
		}
	}
	
	private function adicionarEntidadeArquivo($documento,$entidade,$id){
		try{
            $sql = "INSERT INTO entity_has_document(documents,meta_entity,entity) VALUES (:documents,:meta_entity,:entity)";

        	$conn = new DbConnector();
            $stmt = $conn->getConn()->prepare($sql);
        	$stmt->bindParam(':documents', $documento);
			$stmt->bindParam(':meta_entity', $entidade);
			$stmt->bindParam(':entity', $id);
    		$result = $stmt->execute(); 
			return $result;
		  
		}catch(PDOException $e)
		{
			var_dump($e);
			return $result;
		}
	}
	
	private function procurarTipoDocumento($tipo){
	
		try{
             $sql = "SELECT id FROM doc_type WHERE name = :tipo";

        	$conn = new DbConnector();
            $stmt = $conn->getConn()->prepare($sql);
        	$stmt->bindParam(':tipo', $tipo);
    		$stmt->execute(); 
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			return $result->id;
		  
		}catch(PDOException $e)
		{
			var_dump($e);
			return $result;
		}
		
	}
	
	private function retornarUltimoDocumento(){
		try{
             $sql = "SELECT * FROM documents ORDER BY id DESC LIMIT 1";

        	$conn = new DbConnector();
            $stmt = $conn->getConn()->prepare($sql);
        	$stmt->bindParam(':tipo', $tipo);
    		$stmt->execute(); 
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			return $result->id;
		  
		}catch(PDOException $e)
		{
			var_dump($e);
			return $result;
		}
	}
	
	
}