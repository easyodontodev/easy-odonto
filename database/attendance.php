<?php
   date_default_timezone_set('America/Sao_Paulo');
 	
	include_once "conection.php";
	include_once "../model/patient.php";
	include_once "../model/hospital.php";
	include_once "../model/employee.php";
	
	
	
	class AttendanceDb
	{
			
		public function add($attendance){

			try{
				$sql = "INSERT INTO attendance (patient, hospital, bed,status, admission_date_itu, doctor_responsible,initial_date)
										VALUES (:patient, :hospital, :bed,:status, :admission_date_itu, :doctor_responsible,:initial_date)";
				
				$gaPatient = $attendance->getPatient();
				$gaHospital = $attendance->getHospital();
				$gaLeito = $attendance->getBed();
				$gaAdmDate =  date('Y-m-d H:i');
				$gaEmployee = $attendance->getDoctor();
				$gaStatus = 1;
				
				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':patient', $gaPatient);
				$stmt->bindParam(':hospital', $gaHospital);
				$stmt->bindParam(':bed', $gaLeito);
				$stmt->bindParam(':status', $gaStatus);
				$stmt->bindParam(':admission_date_itu', $gaAdmDate);
				$stmt->bindParam(':initial_date', $gaAdmDate);
				$stmt->bindParam(':doctor_responsible', $gaEmployee);
				
				$result = $stmt->execute();
				//var_dump($result);
				//var_dump($attendance);
				//die(); 
				return $result;
			}
			catch(PDOExeption $e){
				return $result;
			}
		} 
		
		public function addCauseAdmission($admission_cause,$attendance){
			try{
				$sql = "INSERT INTO attendance_cause (attendance,admission_cause)
				VALUES (:attendance, :admission_cause)";
				
				$adm = $admission_cause;
				$att = $attendance;
				
				
				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':attendance', $att);
				$stmt->bindParam(':admission_cause', $adm);
				
				$result = $stmt->execute();
				return $result;
			}
			catch(PDOExeption $e){
				return $result;
			}
		} 
		
		public function searchCountStatus() {
			
			try {
				$sql = "SELECT count(a.status) as count,s.status as name FROM attendance a right join stats s on a.status = s.id GROUP BY s.status,a.status";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchCountAdmissionCause() {
			
			try {
				$sql = "SELECT ac.type as name,count(ac.type) as count FROM admission_cause ac INNER JOIN attendance_cause a ON ac.id = a.admission_cause GROUP BY ac.type";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		
		
		public function searchPatient($patient) {
			
			try {
				$sql = "SELECT * FROM attendance WHERE patient = :patient";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':patient', $patient);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchPatientId($filter) {
			
			try {
				$sql = "SELECT * 
				FROM attendance a 
				WHERE a.id = :id";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':id', $filter);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchHistoric($filter){
			try {
				$sql = "SELECT a.description,CASE WHEN length(CONCAT( DAY(a.time),'/',month(a.time),'/',year(a.time),' ',hour(a.time),':',minute(a.time),':',second(a.time))) = 19
				THEN CONCAT( DAY(a.time),'/',month(a.time),'/',year(a.time),' ',hour(a.time),':',minute(a.time),':',second(a.time)) ELSE 
				CONCAT( DAY(a.time),'/',month(a.time),'/',year(a.time),' ',hour(a.time),':',minute(a.time),':0',second(a.time)) END AS time,s.status
				FROM appointment a INNER JOIN stats s ON a.status = s.id WHERE a.attendance = :id ORDER BY a.id ASC;";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':id', $filter);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
		}
		
		public function searchIdWithEvaluation($filter) {
			
			try {
				$sql = "SELECT a.id,a.status,p.name as namePatient,
				 u.name as nameDoctor, 
				h.name as nameHospital, b.number_itu as leito,b.itu as uti,i.name_itu,e.*
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN itu_bed b ON a.bed = b.id
				INNER JOIN hospital_itu i ON i.id = b.itu
                LEFT JOIN evaluation e ON e.patient = p.id
				WHERE a.id = :id";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':id', $filter);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchId($filter) {
			
			try {
				$sql = "SELECT a.id,a.status,p.name as namePatient,
				 u.name as nameDoctor, 
				h.name as nameHospital, b.number_itu as uti,i.name_itu
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN itu_bed b ON a.bed = b.id
				INNER JOIN hospital_itu i ON i.id = b.itu
				WHERE a.id = :id";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':id', $filter);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchLast() {
			$hospital = $_SESSION['hospital'];
			try {
				$sql = "SELECT a.id,a.status,p.name as namePatient,
				 u.name as nameDoctor, 
				h.name as nameHospital, b.number_itu as uti,i.name_itu
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN itu_bed b ON a.bed = b.id
				INNER JOIN hospital_itu i ON i.id = b.itu
				WHERE a.hospital = :idHospital ORDER BY ID DESC LIMIT 1";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':idHospital', $hospital->id);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchLastComplete() {
			
			try {
				$sql = "SELECT a.id,a.admission_date_itu,u.name as nameUser,u.cro as croUser,
				h.name as nameHospital,h.nome_chefe_uti,h.telephone_chefe_uti,
				h.telephone_uti,l.name_bed,l.number_itu,hi.name_itu,s.status,ac.type,p.name as namePatient,
				
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN itu_bed l ON a.leito = l.id
				INNER JOIN hospital_itu hi ON hi.id = l.itu
				INNER JOIN stats s ON a.status = s.id
				INNER JOIN admission_cause ac on ac.id = a.admission_cause
				WHERE a.hospital = :idHospital ORDER BY ID DESC LIMIT 1";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				
				return $result;
				
			}
			
			catch(PDOExeption $e) {
				return $result;
				
			}
			
		}
		
		public function searchAll($idHospital) {
			
			try {
				$sql = "SELECT a.id,a.status,p.name as namePatient,

			    u.name as nameDoctor, h.name as nameHospital, b.number_itu as uti,i.name_itu

				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN itu_bed b ON a.bed = b.id
				INNER JOIN hospital_itu i ON i.id = b.itu
				WHERE a.hospital = :idHospital";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':idHospital',$idHospital);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $result;
			}
			catch(PDOExeption $e) {
				return $result;
			}
		}
		
		public function searchAllComplete($idHospital) {
			
			try {
				$sql = "SELECT a.id,a.admission_date_itu,u.name as nameUser,u.cro as croUser,
				h.name as nameHospital,h.nome_chefe_uti,h.telephone_chefe_uti,
				h.telephone_uti,l.name_bed,l.number_itu,hi.name_itu,s.status,ac.type,p.name as namePatient,
				
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN itu_bed l ON a.leito = l.id
				INNER JOIN hospital_itu hi ON hi.id = l.itu
				INNER JOIN stats s ON a.status = s.id
				INNER JOIN admission_cause ac on ac.id = a.admission_cause
				WHERE a.hospital = :idHospital";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':idHospital',$idHospital);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $result;
			}
			catch(PDOExeption $e) {
				return $result;
			}
		}
		
		public function search($idHospital,$text) {
			
			try {
				$textLike = '%'.$text.'%';
				$sql = "SELECT a.id,a.status,p.name as namePatient,
				 u.name as nameDoctor, 
				h.name as nameHospital, b.number_itu as uti,i.name_itu
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN itu_bed b ON a.bed = b.id
				INNER JOIN hospital_itu i ON i.id = b.itu
				WHERE a.hospital = :idHospital AND p.name LIKE :textLike";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':idHospital',$idHospital);
				$stmt->bindParam(':textLike',$textLike);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $result;
			}
			catch(PDOExeption $e) {
				return $result;
			}
		}
		
		public function searchComplete($idHospital,$text) {
			
			try {
				$textLike = '%'.$text.'%';
				$sql = "SELECT a.id,a.admission_date_itu,u.name as nameUser,u.cro as croUser,
				h.name as nameHospital,h.nome_chefe_uti,h.telephone_chefe_uti,
				h.telephone_uti,l.name_bed,l.number_itu,hi.name_itu,s.status,ac.type,p.name as namePatient,
				
				FROM attendance a INNER JOIN patient p ON a.patient = p.id
				INNER JOIN users u ON a.doctor_responsible = u.id
				INNER JOIN hospital h ON a.hospital = h.id
				INNER JOIN itu_bed l ON a.leito = l.id
				INNER JOIN hospital_itu hi ON hi.id = l.itu
				INNER JOIN stats s ON a.status = s.id
				INNER JOIN admission_cause ac on ac.id = a.admission_cause
				WHERE a.hospital = :idHospital AND p.name LIKE :textLike";
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':idHospital',$idHospital);
				$stmt->bindParam(':textLike',$textLike);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $result;
			}
			catch(PDOExeption $e) {
				return $result;
			}
		}
		
		public function editStatus($attendance) {
			
			
			try {
				
				$sql = "UPDATE attendance SET status = :status WHERE id = :id";
				
				$gaStatus = $attendance->getAttendanceStatus();
				$gaId = $attendance->getId();
				
				$conn = new Dbconnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':status', $gaStatus);
				$stmt->bindParam(':id', $gaId);
				$result = $stmt->execute();
				return $result;
			}
			catch(PDOExeption $e) {
				return $result;
			}
		}
		
		public function edit($attendance) {
			
			
				
				$sql = "INSERT INTO `appointment` 
				(`description`,
				`status`,
				`attendance`,
				`time`)
				
				VALUES(
				:description,
				:status,
				:attendance,
				:time)     
				";
				
				
				$gaDescription = $attendance->getDescription();
				
				$gaState = $attendance->getState();
				
				$gaAttendanteId =  $attendance->getAttendanceId();
				date_default_timezone_set('Brazil/East');
				$time = date('Y-m-d H:i:s');
				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':description',$gaDescription);
				$stmt->bindParam(':status', $gaState);
				$stmt->bindParam(':attendance',$gaAttendanteId);
				$stmt->bindParam(':time', $time);
			try {
				$result = $stmt->execute();
				return $result;
			}
			catch(PDOException $e) {
				var_dump($e);
				die();
				return $result;
			}
			
		}
		
		public function addStateMonitoring($id) {
			
			
				
				$sql = "INSERT INTO `appointment` 
				(`description`,
				`status`,
				`attendance`,
				`time`)
				
				VALUES(
				:description,
				:status,
				:attendance,
				:time)     
				";
				
				date_default_timezone_set('Brazil/East');
				$time = date('Y-m-d H:i:s');
				$description = '[{"name":"","date":"","number":"1"}]';
				$state = "9";
				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':description',$description);
				$stmt->bindParam(':status', $state);
				$stmt->bindParam(':attendance',$id);
				$stmt->bindParam(':time', $time);
			try {
				$result = $stmt->execute();
				return $result;
			}
			catch(PDOException $e) {
				var_dump($e);
				die();
				return $result;
			}
			
		}
		
		public function updateMonitoring($id,$description) {
			
			
				
				$sql = "UPDATE `appointment` SET
				`description` = 
				:description 
				WHERE id = :id  
				";
				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':description',$description);
				$stmt->bindParam(':id',$id,PDO::PARAM_INT);
			try {
				$result = $stmt->execute();
				return $result;
			}
			catch(PDOException $e) {
				var_dump($e);
				die();
				return $result;
			}
			
		}
		
		public function searchMonitoring($id) {
			
			
				
				$sql = "SELECT * FROM appointment
				WHERE attendance = :id AND
				status = 9 ORDER BY date DESC LIMIT 1;     
				";
				$conn = new DbConnector();
				$stmt = $conn->getConn()->prepare($sql);
				$stmt->bindParam(':id',$id);
			try {
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_OBJ);
				return $result;
			}
			catch(PDOException $e) {
				var_dump($e);
				die();
				return $result;
			}
			
		}
		
	}
	
	
