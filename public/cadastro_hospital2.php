<?php 
  require_once "cabecalho.php";
?>
<style type="text/css">
  body{
    width:100%;
    height:160% 
     }
</style>

  <div style="width:100%;height:120%;display: flex;align-items: center;justify-content:center"> <!-- Comeco da tela de login -->
    <div style="background:white;height:90%;width:80%;box-shadow: 1px 1px 8px 1px rgba(0,0,0,0.31);border-radius:5px"><!-- Contorno da tela -->
      <div style="height:25%;background:#08B2FF;padding:30px;"><!-- Cabecalho -->
        <h1 style="color:white;font-size:40">Cadastro de Hospital</h1>
      </div><!-- Fim cabecalho -->

      <div style="height:65%;background:white;padding:30px;"><!-- Corpo -->
        <div class="row">
          <form class="col s12">
            <div class="row">
              <div class="input-field col s6">
                <i style="opacity:50" class="material-icons prefix">local_hospital</i>
                <input id="icon_prefix" type="text" class="validate">
                <label for="icon_prefix">Nome do Hospital</label>
              </div>
              <div class="input-field col s6">
                <i class="material-icons prefix">phone</i>
                <input id="icon_telephone" type="tel" class="validate">
                <label for="icon_telephone">Telefone da UTI</label>
              </div>
              <div class="input-field col s6">
                <i class="material-icons prefix">account_circle</i>
                <input id="icon_prefix" type="text" class="validate">
                <label for="icon_prefix">Nome do Chefe da UTI</label>
              </div>
              <div class="input-field col s6">
                <i class="material-icons prefix">phone</i>
                <input id="icon_telephone" type="tel" class="validate">
                <label for="icon_telephone">Telefone do Chefe da UTi</label>
              </div>
              <div class="utis">
              <div class="uti">
             <!--NOME DA UTI-->   
              <div style="" class="input-field col s6 ">
                <i class="material-icons prefix">business</i>
                <input name="nome_uti[]" id="nome_uti" title="Digite o nome da UTI" type="text" class="validate" required>
                <label for="nome_uti">Nome da UTI</label>
              </div>
            <!--QUANTIDADE DE LEITO DA UTI-->   
              <div style="" class="input-field col s6">
                <i class="material-icons prefix">home</i>
                <input name="quantidade_leito_uti[]" id="quantidade_leito_uti" title="Digite quantidade de leitos da UTI" pattern="^\S[0-9]*$" type="text" class="validate" required>
                <label for="quantidade_leito_uti">Quantidade de leitos na UTI</label>
              </div>
              <!--ADD UTI-->
              <div class="input-field col s10">
                <a id="addUTI" class="btn-floating blue positionStatic"> <i class="material-icons">add</i></a>
                <a id="delUTI" class="btn-floating red positionStatic"> <i class="material-icons">delete</i></a>
              </div>
               <!--ARQUIVOS-->
              <div style="" class="col s6">
              <div style="" class="file-field input-field col s12" id="hospitalFile">
                <div class="btn input-field col s4 light-blue">
                  <span>Arquivos</span>
                   <input type="file" name="arquivos[]" multiple>
                </div>
                <div class="file-path-wrapper col s8">
                  <input class="file-path validate" type="text" placeholder="Upload de arquivos">
                </div>
              </div>
            </div>
              <!--IMAGEM-->
              <div class="col s6">
              <div style="" class="file-field input-field col s12" id="hospitalFile">
                <div class="btn input-field col s4 light-blue ">
                  <span>Imagem</span>
                  <input type="file" name="imagemHospital" accept="image/*">
                </div>
                <div class="file-path-wrapper col s8">
                  <input class="file-path validate" type="text" placeholder="Upload de uma imagem">
                </div>
              </div>
              </div>
              </div>
              </div>
            </div>
          </form>
        </div>
      </div><!-- Fim corpo -->

      <div style="height:10%;background-color:#03a9f4;padding:0px;margin:0px" class="row"><!-- Footer -->

          <div class="col s6" style="height:100%;width:50%;background:#3C3E3F;display: flex;align-items: center;">
            <a class="" style="color: #fafafa;font-size:15px;margin-left:20px">Cancelar</a>
          </div>

          <div class="col s6" style="height:100%;width:50%;background:#007EE5;display: flex;align-items: center;">
            <button href="#!" onclick="editAttendanceSubmit('formEditAttendanceAvaliation')" class="buttonEditAttendance modal-action modal-close" disabled style="border: none;background-color: transparent;color: white;color:#fafafa;font-size:15px;margin-right:20px;margin-left: auto;">Confirmar Cadastro</button>
          </div>

      </div><!-- Fim footer -->
      
    </div><!-- Fim do contorno -->
  </div>  <!-- Fim da tela de login -->

   
 <?php require_once "rodape.php";
