<?php
	require_once "cabecalho.php";

	require_once "../database/hospital.php";
	require_once "../database/attendance.php";
	require_once "../controller/attendance.php";
	require_once "../controller/uti.php";
	require_once "../controller/session.php";

	
	$hospital = "";
	
	// BUSCANDO HOSPITAL SELECIONADO E SEPARANDO SEU NOME
	$session = new Session();
	$hospital = $session->sessionHospital();
	$hospitalName = $hospital->name;
	
	// USANDO HOSPITAL SELECIONADO PARA BUSCAR UTIS
	$utiController = new UtiController();
	$utis = $utiController->returnUtis($hospital);

	// BUSCANDO TODOS ATENDIMENTOS CADASTRADOS
	$attendanceController = new AttendanceController();
	$attendances = $attendanceController->searchAll($hospital);

?>

<?php require_once "atendimentos/avaliacaoModal.php"; ?>

<?php require_once "atendimentos/reavaliacaoModal.php"; ?>
	
<?php require_once "atendimentos/aprovacaoModal.php"; ?>

<?php require_once "atendimentos/orcamentoModal.php"; ?>

<?php require_once "atendimentos/acompanhamentoModal.php"; ?>

<?php require_once "atendimentos/concluidoModal.php"; ?>







<script>
	$(onPageLoad);
</script>

<?php require_once "atendimentos/apresentacaoHospital.php"; ?>



	<div id="modalCadPatient" style="width:60%;" class="modal">
        <div class="modal-content">
        </div>
    </div>



<?php require_once "atendimentos/cadastroModal.php"; ?>


<div class="main margemCentro" style="width:84%">

  <!--SEARCH BAR-->
	<div>
		<ul>
			<div style="float:left;bottom:10px;position:relative">
				<a href="#modalCadAttendance"><button id="botaoAddAttendance">Adicionar Paciente</button></a>
			</div>

			<div style="left:60%;float:right;position:absolute;width:280px;top:-5px">
				<input id="pesquisa" type="text">
				<a id="botaoPesquisar" class="btn-floating blue positionStatic"> <i class="material-icons">search</i></a>
			</div>
		</ul>
	</div>



  <!-- BOARD-->

<?php require_once "atendimentos/board.php"; ?>



</div>



<?php require_once "rodape.php";
