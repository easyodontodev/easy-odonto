<div id="boardAttendance" style="clear:both">
	<div class="column" id="avaliacao">
		<div style="background-color: #44BBFF;color:white" class="portlet column-title lighten-2 col s10 center" >Avaliação</div>
	</div>

	<div class="column" id="reavaliacao">
		<div style="background-color:#9575cd;color:white" class="portlet column-title lighten-2 col s10 center">Reavaliação</div>
	</div>

	<div class="column" id="aprovacao">
		<div style="background-color:#4db6ac;color:white" class="portlet column-title lighten-2 col s10 center">Aprovação</div>
	</div>

	<div class="column" id="orcamento">
		<div style="background-color:#ef5350;color:white" class="portlet column-title lighten-2 col s10 center">Orçamento</div>
	</div>

	<div class="column" id="acompanhamento">
		<div style="background-color:#2980b9;color:white" class="portlet column-title lighten-2 col s10 center">Acompanhamento</div>
	</div>

	<div class="column" id="concluido">
		<div style="background-color:#66CC99;color:white" class="portlet column-title lighten-2 col s10 center">Concluído</div>
	</div>
</div>