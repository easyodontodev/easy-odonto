<style type="text/css">
	.uticlass .select-wrapper input.select-dropdown{
    	border-bottom: 1px solid #ffffff;
	}
</style>

<div style="border-radius:5px;" id="editAttendanceModal" class="modal ">
	<div style="background-color:#009AE0;">

		<div style="height:60%;background:#08B2FF;padding:30px;">		
			<div class="row">
				<div style="color:white;" class="col s6">
					<i style="font-size:30;position:relative;top:5px;" class="material-icons">account_circle</i>
          			<label style="color:white;font-size:16px;display:inline-block;margin-bottom:2px;letter-spacing:0.3px;margin-left:10px" class="nome-pesquisa-modal"></label><br>
				</div>

				<div style="color:white;" class="col s6">
					<i style="font-size:30;position:relative;top:5px;" class="material-icons">supervisor_account</i>
					<label style="color:white;font-size:16px;display:inline-block;margin-bottom:2px;letter-spacing:0.3px;margin-left:10px" class="doctor-responsavel-modal"></label><br>
          			
				</div>

			
				<div style="color:white;margin-top:20px;font-weight:500;" class="col s6">
					<i style="font-size:30;position:relative;top:5px; float: left;" class="material-icons">local_hospital</i>
					<div style="margin-top: -5px;" class="uticlass input-field col s">
    					<select id="uti">
							<option value="n" disabled selected>UTI</option>
							<?php foreach($utis as $uti){?>
							<option value="<?=$uti->id?>"><?=$uti->name_itu?></option>
							<?php } ?>
    					</select>
  					</div>
          			
				</div>

				<div style="color:white;margin-top:20px;font-weight:500;" class="col s6">
					<i style="font-size:30;position:relative;top:5px;float: left" class="material-icons">airline_seat_flat_angled</i>
					<div style="margin-top: -5px;" class="uticlass input-field col s8">
    					<select name="leito" id="leito">
							<option value="n" disabled selected>Leito</option>
    					</select>
  					</div>
          			
				</div>
			</div>
		</div>	

		<div class="row"> <!-- Comeco das abas -->
	
				<div class="col s4" onclick="mudarAbas(1)" style="cursor:pointer;height:50px;background:#0098DD;display: flex;align-items: center;justify-content:center;">
		 			<a style="color: #fafafa;font-size:15px;margin-left:20px">Principal</a>
				</div>
				<div class="col s4" onclick="mudarAbas(2)" style="cursor:pointer;height:50px;background:#019EE5;display: flex;align-items: center;justify-content:center;">
		  			<a style="color: #fafafa;font-size:15px;margin-left:20px">Indicadores</a>
				</div>
				<div class="col s4" onclick="mudarAbas(3)" style="cursor:pointer;height:50px;background:#019EE5;display: flex;align-items: center;justify-content:center;">
		 			<a  style="color: #fafafa;font-size:15px;margin-left:20px">Histórico</a>
				</div>
		</div>

		  <!--<button href="#!" class="modal-action modal-close" style="float:right;border:none;background-color:#03a9f4;color:white;margin:8px;text-align:center;">X</button>-->
	</div>
	<div style="" class="modal-content">
		<div class="conteudoPrincipal">
			<form action="radios" id="formEditAttendanceAvaliation">

				<input type="hidden" name="attendanceId" class="attendanceId">
		 			<div class="row">
						<p>
							<div class="col s4">
								<input name="stateAttendance" type="radio" value="3" onclick="checkAprovation()" id="test1"/>
								<label for="test1">Aprovação</label><br>
							</div>
							<div class="col s4">
								<input name="stateAttendance" type="radio" value="2" onchange="checkReavaliation()" id="test2" />
								<label for="test2">Reavaliação</label><br>
							</div>
							<div class="col s4">
								<input name="stateAttendance" type="radio" value="13" id="test3" onchange="checkConclude()"  />
								<label for="test3">Concluído</label>
							</div>
						</p>
					</div>	

				<div id="checkAprovation">
				</div>
				<div id="checkReavaliation">
				</div>
				<div id="checkConclude">
				</div>

			</form>
			
		</div>
		<div class="conteudoIndicadores" style="display:none">
			<?php include "atendimentos/indicadoresModal.php"; ?>
		</div>

		<div class="conteudoHistorico" style="display:none">
			<?php include "atendimentos/historicoModal.php"; ?>
		</div>
	</div>

	<div class="conteudoPrincipal">
		<div style="background-color:#03a9f4;padding:0px;margin:0px" class="modal-footer row">

			<div class="col s6" style="height:100%;width:50%;background:#3C3E3F;display: flex;
		align-items: center;">
			 <a class="modal-action modal-close" style="color: #fafafa;font-size:15px;margin-left:20px">Cancelar</a>
			</div>

			<div class="col s6" style="height:100%;width:50%;background:#007EE5;display: flex;
		align-items: center;">
			  <button href="#!" onclick="editAttendanceSubmit('formEditAttendanceAvaliation')" class="buttonEditAttendance modal-action modal-close" disabled style="border: none;background-color: transparent;color: white;color:#fafafa;font-size:15px;margin-right:20px;margin-left: auto;">Prosseguir Atendimento</button>
			</div>

		</div>
	</div>
	<div class="conteudoIndicadores" style="display:none">
		<div style="background-color:#03a9f4" class="modal-footer">
			<div>   
				<button onclick="addIndicadores()" class="addIndicadores" style="border:none;background:none;float:right;">
					<a href="#!" id="indicadoresbtn" class="modal-action modal-close waves-effect waves-blue btn-flat"  style="background-color:#0277bd; color:#fafafa;" disabled select>Salvar</a></button>
				<button style="border:none;background:none;float:right;"><a class="modal-action modal-close waves-effect waves-blue btn-flat" style="background-color:#03a9f4; color: #fafafa;left:105px;">Fechar</a></button>
			</div>  
		</div>
	</div>


		<div class="conteudoHistorico" style="display:none">
		<div style="background-color:#03a9f4" class="modal-footer">
		<div>
  			<button style="border:none;background:none;float:right;">
  				<a class="modal-action modal-close waves-effect waves-blue btn-flat" style="background-color:#03a9f4; color: #fafafa;left:105px;">Fechar</a>
  			</button>
		</div>
		</div>
		</div>
	
	
</div>