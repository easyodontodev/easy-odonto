<?php
	
	require_once "../controller/patient.php";

	$paciente = new PatientController();
	$planos = $paciente->retornaPlanos();
	
?>

<style>
	.picker__holder{
	    height: 30px;
	  }
	.picker__date-display {
  		background-color:#2196f3;
	}
	.picker__weekday-display {
		  background-color:#2196f3;
		  color:#ffffff;
	}
	.picker__day--selected, .picker__day--selected:hover, .picker--focused .picker__day--selected {
		  background-color:#2196f3;
	}
</style>

<!--MODAL-->
<div id="modalCadAttendance" style="width: 75%; z-index: 1003; display: none; opacity: 1; transform: scaleX(1); top: 10%;" class="modal">
	<div style="background-color:#03a9f4;height:100px;">
		<h3 style="position:relative;top:50%;transform:translateY(-50%);text-align:center;color:white;font-size:250%" class="">Cadastro de Atendimento</h3>
	</div>
	<div class="modal-content">
	</div>


	<!--CLASS/FORM-->
	<div class="row">
		<form class="col s11 m12" id="formAddAttendance" autocomplete="off" required>

			<!--PACIENTE-->

			<div class="input-field col s6"> 
				<i class="material-icons prefix">account_circle</i>
				<input name="patient" id="patient" pattern="^\S[a-zA-ZÀ-úẽẼ\s]+$" title="Apenas Letras" type="text" class="validate" required>
				<label for="patient">Nome Paciente</label>
			</div>
			<!--DATA/NASCIMENTO-->
			<div class="input-field col s6">
				<i class="material-icons prefix">today</i>
				<input name="birthdaydate" onchange="valideModalAddAttendance()" id="birthdaydate" type="date" data-date-format="MM/DD/YYYY" class="datepicker" required>
				<label for="birthdaydate">Data de Nascimento Paciente</label>
			</div>

			<!--CPF-->
			<div class="input-field col s6">
				<i class="material-icons prefix">web</i>
				<input name="cpf" id="cpf" title="000.000.000-00" type="text" class="validate"  required>
				<label for="cpf">CPF Paciente</label>
			</div> 

			<!--Genero-->
			<div class="input-field col s6">
				<i class="material-icons prefix">supervisor_account</i>
				<select name="gender" onchange='valideModalAddAttendance();' id="gender">
					<option value="n" disabled selected>Gênero</option>
					<option value="Masculino">Masculino</option>
					<option value="Feminino">Feminino</option>
				</select>
				<label for = "gender">Gênero</label>
			</div>

			<!--PLANO DE SAÚDE-->
			<div class="input-field col s6">
				<i class="material-icons prefix">payment</i>
				<select name="plano_de_saude" onchange='valideModalAddAttendance()' id="plano_de_saude">
					<option value="n" disabled selected>Plano de Saúde Paciente</option>
					<?php foreach($planos as $plano){?>
					<option value="<?=$plano->id?>"><?=$plano->description?></option>
					<?php } ?>
				</select>
				<label for="plano_de_saude">Plano de Saúde Paciente</label>
			</div>
			
			<!--CONDICAO SISTEMICA-->
			<div class="input-field col s6">
				<i class="material-icons prefix">local_hospital</i>
				<select multiple name="admission_cause" id="admission_cause">
					<option value="n" disabled selected>Condição Sistêmica</option>
					<option value="1">Oncológico</option>
					<option value="2">Cardiopata</option>
					<option value="3">Cirurgia</option>
					<option value="4">Paliativo</option>
					<option value="5">Urgente</option>
					<option value="6">Quimioterapia</option>
					<option value="7">Pulmonar</option>
					<option value="8">IRC</option>
					<option value="9">Neuropata</option>
				</select>
				<label for = "admission_cause">Condição Sistêmica</label>
			</div>

			<!--HOSPITAL-->
			<div class="input-field col s6">
				<i class="material-icons prefix">business</i>
				<input name="hospital" id="hospital" style="border-bottom: 1px solid #9e9e9e;color:inherit;" aria-controls="example" type="text" value="<?=$hospitalName?>" required disabled>
				<label for="hospital" style="color: #9e9e9e;">Hospital</label>
			</div>

			<!--UTI-->
			<div class="input-field col s6">
				<i class="material-icons prefix">business</i>
				<select name="uti" onchange='valideModalAddAttendance();procurarLeito(this.value,"leitoCad")' id="uti">
					<option value="n" disabled selected>UTI</option>
					<?php foreach($utis as $uti){?>
					<option value="<?=$uti->id?>"><?=$uti->name_itu?></option>
					<?php } ?>
				</select>
				<label for = "uti">UTI</label>
			</div>
			
			

			<!--LEITO-->
			<div class="input-field col s6">
				<i class="material-icons prefix">business</i>
				<select name="leito" onchange="valideModalAddAttendance()" id="leitoCad">
					<option value="n" disabled selected>Leito</option>
				</select>
				<label for="leito">Leito</label>
			</div>

			<!--Dentista/RESPONSAVEL-->
			<div class="input-field col s6" style="clear:both">
				<i class="material-icons prefix">supervisor_account</i>
				<input name="user" id="user" onchange="valideModalAddAttendance()" autocomplete="off" oninput="procurarUser()" aria-controls="example" type="text" required>
				<input name="Iduser" id="Iduser" type="hidden" required>
				<label for = "user">Dentista Responsável</label>
				<div id="mostramedico" class="spacewhite"></div>
			</div>

			<!--MEDICO DO RESPONSÁVEL-->
			<div class="input-field col s6" style="margin-top:-65px">
				<i class="material-icons prefix">supervisor_account</i>
				<input name="doctor" id="doctor" pattern="^\S[a-zA-ZÀ-úẽẼ\s]+$" title="Apenas Letras" type="text" class="validate" required>
				<label for="doctor">Médico Responsável</label>
			</div>
		</form>
	</div>
	<a href="#!" id="adicionar" onclick="addAttendanceModal()" disabled class="modal-action modal-close waves-effect waves-blue btn-flat" style="float:right; position:relative; margin-bottom:10px;margin-right:6px; top:-27px;border: 2px;padding: 0px 15px;text-transform: none;border: 2px solid #03a9f4;background-color: #03a9f4; color: #fafafa">Cadastrar</a>

</div>