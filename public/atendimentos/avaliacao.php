<div id="editAttendanceModal" class="modal">
<div style="background-color:#03a9f4;padding:28px;">
	  <h3 style="font-size:35px;display:inline;color:white;letter-spacing:2px">Status Avaliação</h3>
	  <!--<button href="#!" class="modal-action modal-close" style="float:right;border:none;background-color:#03a9f4;color:white;margin:8px;text-align:center;">X</button>-->
</div>
	<div style="" class="modal-content">
	<div class="">
	  
	  </div>
	  <div>
		  <form action="radios" id="formEditAttendanceAvaliation">
			<input type="hidden" name="attendanceId" class="attendanceId">
			<div style="width:50%;float:left;"><label style="font-size: 17px;color: #03a9f4;">Paciente: </label><label style="font-size: 15px;color: #555555" class="nome-pesquisa-modal"></label></div>
			<div style="width:30%;float:left;"><label style="font-size: 17px;color: #03a9f4;">UTI: </label><label style="font-size: 15px;color: #555555" class=" nme-uti-modal" ></label></div>
			<div style="width:20%;float:left;"><label style="font-size: 17px;color: #03a9f4;">Leito: </label><label style="font-size: 15px;color: #555555" class="nme-leito-modal" ></label></div>
			<div style="width:100%;float:left;"><label style="font-size: 17px;color: #03a9f4;">Doutor responsável: </label><label style="font-size: 15px;color: #555555" class="doctor-responsavel-modal" ></label></div>
			<p style="margin-bottom:17px;margin-top:13px;clear:both">
				<br>
			  <input name="stateAttendance" type="radio" value="3" onchange="checkAprovation()" id="test1"/>
			  <label for="test1">Aprovação</label><br>
			  <input name="stateAttendance" type="radio" value="2" onchange="checkReavaliation()" id="test2" />
			  <label for="test2">Reavaliação</label><br>
			  <input name="stateAttendance" type="radio" value="13" id="test3" onchange="checkConclude()"  />
			  <label for="test3">Concluído</label>
			</p>
			<div id="checkAprovation">
			</div>
			<div id="checkReavaliation">
			</div>
			<div id="checkConclude">
			</div>

		  </form>
	  </div>
	</div>
	<div style="background-color:#03a9f4" class="modal-footer">
		<div>		
		 <button style="border:none;background:none;float:right;"><a href="#!" onclick="editAttendanceSubmit('formEditAttendanceAvaliation')" class="buttonEditAttendance modal-action modal-close waves-effect waves-blue btn-flat" disabled style="background-color:#0277bd; color:#fafafa;">Prosseguir Atendimento</a></button>
	  	 <button style="border:none;background:none;float:right;"><a class="modal-action modal-close waves-effect waves-blue btn-flat" style="background-color:#03a9f4; color: #fafafa;left:105px;">Fechar</a></button>
	  	</div>	
	</div>
  </div>