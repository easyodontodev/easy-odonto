
<form action="radios" id="formIndicadores">
    <input type="hidden" name="attendanceId" class="attendanceId">
    1. Cabeceira se encontra entre 30º e 45º quando entrou no leito?
    <br>
      <input name="cabeceira" value="1" type="radio" id="simCabeceira"onchange="valideModalAddIndicadores()" />
      <label style="right:-20px;" for="simCabeceira">Sim</label>

      <input name="cabeceira" value="2" type="radio" id="naoCabeceira" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-40px;" for="naoCabeceira">Não</label>


    </p>

    <p>
    2. Dieta:
    <br>

      <input name="dieta" value="1" type="radio" id="oral" onchange="valideModalAddIndicadores()"/>
      <label style="right:-20px;" for="oral">Oral</label>

      <input name="dieta" value="2" type="radio" id="mista" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-40px;" for="mista">Mista</label>

      <input name="dieta" value="3" type="radio" id="nao-oral" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-60px;" for="nao-oral">Não Oral</label>
    </p>

    <p>
    3. Higiene Oral pela Enfermagem:
    <br>

      <input name="hope" value="1" type="radio" id="IHOPC" onchange="valideModalAddIndicadores()" />
      <label style="right:-20px;" for="IHOPC">IHOPC</label>

      <input name="hope" value="2" type="radio" id="efetiva" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-40px;" for="efetiva">Efeitiva</label>

     <input name="hope" value="3" type="radio" id="deficiente" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-60px;" for="deficiente">Deficiente</label>

      <input name="hope" value="4" type="radio" id="precaria" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-80px;" for="precaria">Precária</label>
    </p>

    <p>
    4. Tipo de Ventilação:
    <br>

      <input name="ventilacao" value="1" type="radio" id="ambiente" onchange="valideModalAddIndicadores()"/>
      <label style="right:-20px;" for="ambiente">Ar Ambiente</label>

      <input name="ventilacao" value="2" type="radio" id="IOT" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-40px;" for="IOT">IOT</label>

     <input name="ventilacao" value="3" type="radio" id="TQT" onchange="valideModalAddIndicadores()" />
      <label style="margin-top: -20px;right:-60px;" for="TQT">TQT</label>

      <input name="ventilacao" value="4" type="radio" id="outros" onchange="valideModalAddIndicadores()"/>
      <label style="margin-top: -20px;right:-80px;" for="outros">Outros</label>
    </p>

    <p>
    5. Odontologia:
    <br>

      <input name="odontologia" value="1" type="radio" id="acompOdonto" onchange="valideModalAddIndicadores()" />
      <label for="acompOdonto">Necessita de Acompanhamento Odontológico</label>
      <br><br>
      <input name="odontologia" value="2" type="radio" id="HO" onchange="valideModalAddIndicadores()" />
      <label for="HO">Apenas HO pela enf/pac</label>
      <br><br>
      <input name="odontologia" value="3" type="radio" id="atendOdonto"  onchange="valideModalAddIndicadores()"/>
      <label for="atendOdonto">Em atend. pela odonto</label>

    </p>

    <p>
    6. Fatores Odontológicos IHOPC encontrados:
    <br>
    <div style="width:50%;" class="input-field col s6">
    <select multiple name="fatores_odonto" id="fatores_odonto" onchange="valideModalAddIndicadores()">
          <option value="n" disabled selected>Fatores Odontológicos IHOPC encontrados</option>
          <option value="1">Gengivite</option>
          <option value="2">Presença de placa dental/biofilm</option>
          <option value="3">Saburra</option>
          <option value="4">Halitose</option>
          <option value="5">Presença de secreção/crosta</option>
          <option value="6">Presença de sangue</option>
          <option value="7">Presença de restos alimentares (dieta)</option>
          <option value="8">Fatores de retenção</option>
          
        </select>
        <label for = "fatores_odonto"></label>
      </div>

      <input name="nota" value="1" type="radio" id="ho_satisfatorio" onchange="valideModalAddIndicadores()" />
      <label for="ho_satisfatorio">HO Satisfatória - 0-1 pontos</label>
      <br><br>
      <input name="nota" value="2" type="radio" id="ho_deficiente" onchange="valideModalAddIndicadores()"/>
      <label for="ho_deficiente">HO Deficiente - 2-3 pontos</label>
      <br><br>
      <input name="nota" value="3" type="radio" id="ho_precaria" onchange="valideModalAddIndicadores()" />
      <label for="ho_precaria">HO Precária - 4-7 pontos</label>

    </p>


</form>
