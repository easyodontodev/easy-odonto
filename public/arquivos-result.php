<?php
	require_once "../database/data.php";
	
	$arquivos = $_FILES['arquivos'];
	$entidade = $_POST['entidade'];
	$id = $_POST['id'];
	uploadFiles($id,$arquivos,$entidade);
	
	
	function uploadFiles($id,$files,$entidade){
		$db =  new DataDb();
		for($contador=0;$contador<sizeof($files);$contador++){
			if($files['size'][$contador] > 0 ){
				$extensao = strtolower(strrchr($files['name'][$contador],'.'));
				$fileName = $files['name'][$contador];
				$fileName = substr(hash("sha256",date("Y-m-d H:i:s").$files['name'][$contador]),0,12).$extensao;
				$destino = '../arquivos/' .$fileName;
				$arquivo_tmp = $files['tmp_name'][$contador];
				move_uploaded_file( $arquivo_tmp, $destino);
				$db->adicionarArquivo($id,$entidade,$fileName,$destino,$extensao);
			}
		}
	}