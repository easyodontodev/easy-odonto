<?php
	// 23/03/2017
	require_once "../model/attendance_update.php";
	require_once "../model/employee.php";
	require_once "../model/patient.php";
	require_once "../model/attendance.php";
	require_once "../database/attendance.php";
	require_once "../controller/verifyLogin.php";
	require_once "../controller/session.php";
	
	
	
	class AttendanceController{
		
		function __construct(){
			verifyLogRedirect();
		}
		
		public function edit(){
			$conn = new AttendanceDb();
			$AttendanceUpdate = new AttendanceUpdate();
			$AttendanceUpdate->setAttendanceId($_POST['id']);
			$AttendanceUpdate->setState($_POST['state']);
			$AttendanceUpdate->setDescription($_POST['description']);
			$result = $conn->edit($AttendanceUpdate);
			$Attendance = new Attendance();
			if($result){
				$Attendance->setId($AttendanceUpdate->getAttendanceId());
				$Attendance->setAttendanceStatus($AttendanceUpdate->getState());
				$result2 = $conn->editStatus($Attendance);
			}
			if($AttendanceUpdate->getState() == "7"){
				$conn->addStateMonitoring($AttendanceUpdate->getAttendanceId());
			}
			echo $this->redirect($result2,$Attendance->getId());
			
		}
		
		public function searchMonitoring($id){
			$conn = new AttendanceDb();
			return  $conn->searchMonitoring($id);
		}
		
		public function saveMonitoring($id,$description){
			$conn = new AttendanceDb();
			return  $conn->updateMonitoring($id,$description);
		}
		
		public function add($patientId){
			$conn = new AttendanceDb();
			//$patient = $conn->searchPatient($_POST["Idpatient"]);
			
			//if($patient == false || $patient->status == 13){
				
				$Attendance = new Attendance();
				$hosp = $_SESSION['hospital'];
				$Attendance->setPatient($patientId);
				$Attendance->setHospital($hosp->id);
				$Attendance->setBed($_POST["leito"]);
				//$Attendance->setUtiAdmissionDate($_POST["admdate"]);
				$Attendance->setDoctor($_POST["Iduser"]);

				
				$result = $conn->add($Attendance);
				if($result){
					$attendanceLast = $conn->searchLast();
					$admcauses = $_POST["admission_cause"];
					if(is_array($admcauses)){
						foreach($admcauses as $admCauseSingle){
							$conn->addCauseAdmission($admCauseSingle,$attendanceLast->id);
						}
						}else{
						$conn->addCauseAdmission($admcauses,$attendanceLast->id);
					}
				}
				echo $this->redirect($result,0);
			//}
		}
		
		private function redirect($result,$id){
			if($result){
				$conn = new AttendanceDb();
				if($id != 0){
					$attendance = $conn->searchId($id);
				}
				else{
					$attendance = $conn->searchLast();
				}
				return $this->prepare($attendance);
			}
		}
		
		public function searchAll($hospital){
			$db = new AttendanceDb();
			$attendances = $db->searchAll($hospital->id);
			if($attendances != null || $attendances != false){
				foreach($attendances as $attendance){
					echo $this->prepare($attendance);
				}
			}
		}
		
		public function prepare($attendance){

			if($attendance->status == '1'){
				
				return '<script>
				$(document).ready(function(){
				$("#avaliacao").append(\'<div class="item-pesquisa portlet" id="attendance'.$attendance->id.'">\'
				+ \'<div class="nome-pesquisa portlet-header" style="word-wrap: break-word;">'.$attendance->namePatient.'</div>\'
				+ \'<div class="portlet-content" style="word-wrap: break-word;"><div><a onclick="openModalEditAttendance('.$attendance->id.')" class="btn btn-small btn-flat waves-effect waves-light" style="float:right"><i class="material-icons">mode_edit</i></a></div><b>Doutor responsável:</b> <br> <label class="doctor-responsavel">'.$attendance->nameDoctor.'</label>, <br>\'
				+ \'<b>UTI:</b> <label class="nme-uti">'.$attendance->name_itu.'</label>, <br> <b>Leito:</b> <label class="nme-leito">'.$attendance->uti.'</label></div>\'
				+ \'</div>\');
				});</script>';
			}
			if($attendance->status == '2'){
				return '<script>
				$(document).ready(function(){
				$("#reavaliacao").append(\'<div class="item-pesquisa portlet" id="attendance'.$attendance->id.'">\'
				+ \'<div class="nome-pesquisa portlet-header" style="word-wrap: break-word;">'.$attendance->namePatient.'</div>\'
				+ \'<div class="portlet-content" style="word-wrap: break-word;"><div><a onclick="openModalEditAttendanceReavaliation('.$attendance->id.')" class="btn btn-small btn-flat waves-effect waves-light" style="float:right"><i class="material-icons">mode_edit</i></a></div><b>Doutor responsável:</b> <br> <label class="doctor-responsavel">'.$attendance->nameDoctor.'</label>, <br>\'
				+ \'<b>UTI:</b> <label class="nme-uti">'.$attendance->name_itu.'</label>, <br> <b>Leito:</b> <label class="nme-leito">'.$attendance->uti.'</label></div>\'
				+ \'</div>\');
				});</script>';
			}
			if($attendance->status == '3'){
				return '<script>
				$(document).ready(function(){
				$("#aprovacao").append(\'<div class="item-pesquisa portlet" id="attendance'.$attendance->id.'">\'
				+ \'<div class="nome-pesquisa portlet-header" style="word-wrap: break-word;">'.$attendance->namePatient.'</div>\'
				+ \'<div class="portlet-content" style="word-wrap: break-word;"><div><a onclick="openModalEditAttendanceAprovation('.$attendance->id.')" class="btn btn-small btn-flat waves-effect waves-light" style="float:right"><i class="material-icons">mode_edit</i></a></div><b>Doutor responsável:</b> <br> <label class="doctor-responsavel">'.$attendance->nameDoctor.'</label>, <br>\'
				+ \'<b>UTI:</b> <label class="nme-uti">'.$attendance->name_itu.'</label>, <br> <b>Leito:</b> <label class="nme-leito">'.$attendance->uti.'</label></div>\'
				+ \'</div>\');
				});</script>';
			}
			if($attendance->status == '6'){

				return '<script>
				$(document).ready(function(){
				$("#orcamento").append(\'<div class="item-pesquisa portlet" id="attendance'.$attendance->id.'">\'
				+ \'<div class="nome-pesquisa portlet-header" style="word-wrap: break-word;">'.$attendance->namePatient.'</div>\'
				+ \'<div class="portlet-content" style="word-wrap: break-word;"><div><a onclick="openModalEditAttendanceBudget('.$attendance->id.')" class="btn btn-small btn-flat waves-effect waves-light" style="float:right"><i class="material-icons">mode_edit</i></a></div><b>Doutor responsável:</b> <br> <label class="doctor-responsavel">'.$attendance->nameDoctor.'</label>, <br>\'
				+ \'<b>UTI:</b> <label class="nme-uti">'.$attendance->name_itu.'</label>, <br> <b>Leito:</b> <label class="nme-leito">'.$attendance->uti.'</label></div>\'
				+ \'</div>\');
				});</script>';

			}
			if($attendance->status == '7' || $attendance->status == '9'){
				return '<script>
				$(document).ready(function(){
				$("#acompanhamento").append(\'<div class="item-pesquisa portlet" id="attendance'.$attendance->id.'">\'
				+ \'<div class="nome-pesquisa portlet-header" style="word-wrap: break-word;">'.$attendance->namePatient.'</div>\'
				+ \'<div class="portlet-content" style="word-wrap: break-word;"><div><a onclick="openModalEditAttendanceMonitoring('.$attendance->id.')" class="btn btn-small btn-flat waves-effect waves-light" style="float:right"><i class="material-icons">mode_edit</i></a></div><b>Doutor responsável:</b> <br> <label class="doctor-responsavel">'.$attendance->nameDoctor.'</label>, <br>\'
				+ \'<b>UTI:</b> <label class="nme-uti">'.$attendance->name_itu.'</label>, <br> <b>Leito:</b> <label class="nme-leito">'.$attendance->uti.'</label></div>\'
				+ \'</div>\');
				});</script>';
			}
			if($attendance->status == '13'|| $attendance->status == '1'){
				return '<script>
				$(document).ready(function(){
				$("#concluido").append(\'<div class="item-pesquisa portlet" id="attendance'.$attendance->id.'">\'
				+ \'<div class="nome-pesquisa portlet-header" style="word-wrap: break-word;">'.$attendance->namePatient.'</div>\'
				+ \'<div class="portlet-content" style="word-wrap: break-word;"><div><a onclick="openModalEditAttendanceConclude('.$attendance->id.')" class="btn btn-small btn-flat waves-effect waves-light" style="float:right"><i class="material-icons">mode_edit</i></a></div><b>Doutor responsável:</b> <br> <label class="doctor-responsavel">'.$attendance->nameDoctor.'</label>, <br>\'
				+ \'<b>UTI:</b> <label class="nme-uti">'.$attendance->name_itu.'</label>, <br> <b>Leito:</b> <label class="nme-leito">'.$attendance->uti.'</label></div>\'
				+ \'</div>\');
				});</script>';
			}
		}
		
	}
	
